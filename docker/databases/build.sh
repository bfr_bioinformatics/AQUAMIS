#!/bin/bash
set -euo pipefail

# Locate Self
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
repo_path=$(dirname $(dirname $script_path))

# Pull the latest version of the image, in order to populate the build cache:
docker pull bfrbioinformatics/aquamis:databases || true

# Build the databases:
echo "Building bfrbioinformatics/aquamis:databases"
printf -v buildbegin '%(%Y-%m-%d %H:%M:%S)T' -1
docker build  \
  --file $script_path/Dockerfile  \
  --target databases  \
  --cache-from=bfrbioinformatics/aquamis:databases  \
  --build-arg BUILD_DATE=$(date +"%Y%m%d%H%M%S")  \
  --tag bfrbioinformatics/aquamis:databases  \
  $script_path
printf -v buildfinish '%(%Y-%m-%d %H:%M:%S)T' -1

# Build duration on Gandalf: 755s
echo "Building time from $buildbegin till $buildfinish"

# Push the new versions:
echo "Pushing bfrbioinformatics/aquamis:databases"
docker push bfrbioinformatics/aquamis:databases
