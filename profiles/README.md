## How-to install CookieCutter Profiles

```
INSTALL_PATH=<path_to_aquamis>
conda activate aquamis
mamba install cookiecutter
[[ ! -d <path_to_aquamis>/profiles ]] && mkdir -p ${INSTALL_PATH}/profiles
[[ ! -d $HOME/.config ]] && mkdir -p $HOME/.config && cd $HOME/.config
[[ ! -d $HOME/.config/snakemake ]] && ln -s ${INSTALL_PATH}/profiles $HOME/.config/snakemake
cd ${INSTALL_PATH}/profiles
cookiecutter https://github.com/Snakemake-Profiles/slurm.git
    profile_name [slurm]: 
    sbatch_defaults []: 
    cluster_config []: 
    Select advanced_argument_conversion:
    1 - no
    2 - yes
    Choose from 1, 2 [1]: 1
    cluster_name []: 
```


## How to determine available cores

This will yield all online CPU, which is the product of:
NPROCESSORS   =   # of Sockets   *   # of Core(s) per socket   *    Thread(s) per core

```
NPROCESSORS=$(expr $(nproc) / 2)
CORES=${NPROCESSORS%%.*}
```

For more system information check `LANG=C && lscpu`
