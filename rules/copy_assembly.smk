# %% assembly rules -------------------------------------------------

localrules: copy_preassembled_fasta

rule copy_preassembled_fasta:
    input:
        fasta = lambda wildcards: _get_fastx(wildcards,samples_column)
    output:
        contigs = "Assembly/{sample}/contigs.fa",
        fasta = "Assembly/assembly/{sample}.fasta"
    group: rule_directives.get("copy_preassembled_fasta",{}).get("group",None)
    priority: rule_directives.get("copy_preassembled_fasta",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Integrate preassembled FastA into workflow for sample {wildcards.sample}"
    log:
        "logs/{sample}_copy_fasta.log"
    shell:
        """
        cp --verbose {input.fasta} {output.fasta} >> {log}
        cp --verbose {input.fasta} {output.contigs} >> {log}
        """
