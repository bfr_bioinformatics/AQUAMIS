# %% Imports --------------------------------------------------------

import logging
import subprocess
import traceback
import fileinput
from confindr_src import confindr

# %% trimming rule --------------------------------------------------

rule run_fastp:
    input:
        r1 = lambda wildcards: _get_fastx(wildcards,samples_column),
    params:
        length_required = config["params"]["fastp"]["length_required"],
        fastp_extra = "--trim_tail1 1",# trim last base of each read
        n_base_limit = 50  # if one read's number of N base is >n_base_limit, then this read/pair is discarded
    output:
        r1 = "trimmed/{sample}_R1.fastq.gz",
        json = "trimmed/reports/{sample}.json",
        html = "trimmed/reports/{sample}.html"
    group: rule_directives.get("run_fastp",{}).get("group",None)
    priority: rule_directives.get("run_fastp",{}).get("priority",None)
    resources:
        cpus = config["smk_params"]["threads"],
        mem_mb = 4000,
        time = 2
    message:
        "Running Fastp on sample {wildcards.sample}"
    log:
        "logs/{sample}_fastp.log"
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        fastp --verbose --thread {threads} --in1 {input.r1} --out1 {output.r1} --n_base_limit {params.n_base_limit} --json {output.json} --html {output.html} --report_title 'Sample {wildcards.sample}' --length_required {params.length_required} {params.fastp_extra} &> {log}
        """

# %% confindr rule --------------------------------------------------

rule run_confindr:
    input:
        r1 = "trimmed/{sample}_R1.fastq.gz",
    params:
        forward_id = '_R1',
        output_folder = "confindr/{sample}",
        databases_folder = config["params"]["confindr"]["database"],
        tmpdir = config["params"]["confindr"]["database"],
        keep_files = False,
        quality_cutoff = 20,
        base_cutoff = 2,
        base_fraction_cutoff = 0.05,
        data_type = 'Illumina',
        use_rmlst = config["params"]["confindr"]["use_rmlst"],
        cross_details = True,
        min_matching_hashes = 40
    output:
        confindr = "confindr/{sample}/confindr_report.csv"
    group: rule_directives.get("run_confindr",{}).get("group",None)
    priority: rule_directives.get("run_confindr",{}).get("priority",None)
    resources:
        cpus = config["smk_params"]["threads"],
        mem_mb = 4000,
        time = 4
    message:
        "Running Confindr on sample {wildcards.sample}"
    log:
        "logs/{sample}_confindr.log"
    threads:
        config["smk_params"]["threads"]
    run:
        print("Confindr analysis of sample " + wildcards.sample)
        logging.basicConfig(
            filename = log[0],
            format = '\033[92m \033[1m %(asctime)s \033[0m %(message)s ',
            level = logging.DEBUG,
            datefmt = '%Y-%m-%d %H:%M:%S')
        try:
            confindr.find_contamination(
                pair = [input.r1],
                forward_id = params.forward_id,
                threads = threads,
                output_folder = params.output_folder,
                databases_folder = params.databases_folder,
                keep_files = params.keep_files,
                quality_cutoff = params.quality_cutoff,
                base_cutoff = params.base_cutoff,
                base_fraction_cutoff = params.base_fraction_cutoff,
                tmpdir = params.tmpdir,
                data_type = params.data_type,
                use_rmlst = params.use_rmlst,
                cross_details = params.cross_details,
                min_matching_hashes = params.min_matching_hashes)
        except subprocess.CalledProcessError:
            print("Confindr analysis failed! Writing a dummy result.")
            logging.warning('Error encountered was:\n{}'.format(traceback.format_exc()))
            with open(output.confindr,'w',newline = '') as outfile:
                outfile.write('Sample,Genus,NumContamSNVs,ContamStatus,PercentContam,PercentContamStandardDeviation,BasesExamined,DatabaseDownloadDate\n')
                outfile.write(wildcards.sample + ',ND,ND,ND,ND,ND,ND,ND\n')
        for line in fileinput.input(output.confindr,inplace = True):
            print(line.replace("_R1,",","),end = "")

# %% kraken2_reads rules --------------------------------------------

rule run_kraken2_reads:
    input:
        r1 = "trimmed/{sample}_R1.fastq.gz",
    params:
        db_kraken = config["params"]["kraken2"]["db_kraken"]
    output:
        kraken_report = "kraken2/{sample}.fromreads.kreport",
        kraken_out = temp("kraken2/{sample}.fromreads.kraken")
    group: rule_directives.get("run_kraken2_reads",{}).get("group",None)
    priority: rule_directives.get("run_kraken2_reads",{}).get("priority",None)
    resources:
        cpus = config["smk_params"]["threads"],
        mem_mb = 16000,
        time = 4
    message:
        "Running Kraken2 on reads of sample {wildcards.sample}"
    log:
        "logs/{sample}_kraken2_reads.log"
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        kraken2 --threads {threads} --gzip-compressed --db {params.db_kraken} --output {output.kraken_out} --report {output.kraken_report} {input.r1}
        """

rule run_braken_reads_species:
    input:
        "kraken2/{sample}.fromreads.kreport"
    params:
        db_kraken = config["params"]["kraken2"]["db_kraken"],
        read_length = config["params"]["kraken2"]["read_length"],
        taxonomic_level = "S",
        threshold = 0
    output:
        bracken_report = "kraken2/{sample}.species.breport",
        bracken_out_temp = temp("kraken2/{sample}.species.bracken.unsorted"),
        bracken_out = "kraken2/{sample}.species.bracken"
    group: rule_directives.get("run_braken_reads_species",{}).get("group",None)
    priority: rule_directives.get("run_braken_reads_species",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Running Bracken-species on reads of sample {wildcards.sample}"
    log:
        "logs/{sample}_bracken_species.log"
    shell:
        """
        bracken -d {params.db_kraken} -i {input} -o {output.bracken_out_temp} -w {output.bracken_report} -r {params.read_length} -l {params.taxonomic_level} -t {params.threshold} | tee {log} 2> {log}
        awk 'NR<2{{print $0;next}}{{print $0| "sort --field-separator=\\"\t\\" -k7nr -k4nr"}}' {output.bracken_out_temp} > {output.bracken_out}
        """

rule run_braken_reads_genus:
    input:
        "kraken2/{sample}.fromreads.kreport"
    params:
        db_kraken = config["params"]["kraken2"]["db_kraken"],
        read_length = config["params"]["kraken2"]["read_length"],
        taxonomic_level = "G",
        threshold = 0
    output:
        bracken_report = "kraken2/{sample}.genus.breport",
        bracken_out_temp = temp("kraken2/{sample}.genus.bracken.unsorted"),
        bracken_out = "kraken2/{sample}.genus.bracken"
    group: rule_directives.get("run_braken_reads_genus",{}).get("group",None)
    priority: rule_directives.get("run_braken_reads_genus",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Running Bracken-genus on reads of sample {wildcards.sample}"
    log:
        "logs/{sample}_bracken_genus.log"
    shell:
        """
        bracken -d {params.db_kraken} -i {input} -o {output.bracken_out_temp} -w {output.bracken_report} -r {params.read_length} -l {params.taxonomic_level} -t {params.threshold} | tee {log} 2> {log}
        awk 'NR<2{{print $0;next}}{{print $0| "sort --field-separator=\\"\t\\" -k7nr -k4nr"}}' {output.bracken_out_temp} > {output.bracken_out}
        """

# %% pre-assembly json rule -----------------------------------------

rule parse_json_before_assembly:
    input:
        software_versions = "reports/software_versions.tsv",
        db_hashes = "reports/database_versions.md5",
        r1 = lambda wildcards: _get_fastx(wildcards,samples_column),
        trimmed_r1 = "trimmed/{sample}_R1.fastq.gz",
        fastp = "trimmed/reports/{sample}.json",
        confindr_report = "confindr/{sample}/confindr_report.csv",
        kraken_reads_report = "kraken2/{sample}.fromreads.kreport",
        bracken_species_report = "kraken2/{sample}.species.bracken",
        bracken_genus_report = "kraken2/{sample}.genus.bracken"
    params:
        sample = "{sample}",
        config = config
    output:
        json_out = "json/pre_assembly/{sample}.aquamis.json"
    group: rule_directives.get("parse_json_before_assembly",{}).get("group",None)
    priority: rule_directives.get("parse_json_before_assembly",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Aggregating PreAssembly results in JSON format of sample {wildcards.sample}"
    log:
        log = "logs/{sample}_parseJson_beforeAssembly.log"
    script:
        f"{workflow.basedir}/scripts/parse_json.py"

# %% pre-assembly report rule ---------------------------------------

rule write_pre_assembly_report:
    input:
        json_in = expand("json/pre_assembly/{sample}.aquamis.json",sample = samples.index),
        fails = "Assembly/fails.txt"
    params:
        csv_fastp = "reports/summary_fastp.tsv",
        csv_confindr = "reports/summary_confindr.csv",
        csv_bracken = "reports/summary_bracken.tsv"
    output:
        "reports/fastp_report.html"  # TODO: ,str(float(data["adapter_cutting"]["adapter_trimmed_bases"])/float(data["summary"]["before_filtering"]["total_bases"])*100)]
    group: rule_directives.get("write_pre_assembly_report",{}).get("group",None)
    priority: rule_directives.get("write_pre_assembly_report",{}).get("priority",None)
    resources:
        cpus = 1,
        mem_mb = 8000,
        time = 6
    message:
        "Creating QC Rmarkdown report"
    log:
        log = "logs/write_QC_report.log"
    script:
        f"{workflow.basedir}/scripts/write_QC_report.Rmd"  # TODO: Compare collect_bracken with bracken_table, they are not the same!
