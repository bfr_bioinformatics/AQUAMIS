# %% kraken2_contigs rules ------------------------------------------

rule run_kraken2_contigs:
    input:
        contigs = "Assembly/{sample}/contigs.fa"
    params:
        kraken2_db = config["params"]["kraken2"]["db_kraken"],
        kraken_report = "kraken2/{sample}.fromcontigs.kreport"
    output:
        kraken_out = "kraken2/{sample}.fromcontigs.kraken"
    group: rule_directives.get("run_kraken2_contigs",{}).get("group",None)
    priority: rule_directives.get("run_kraken2_contigs",{}).get("priority",None)
    resources:
        cpus = config["smk_params"]["threads"],
        mem_mb = 16000,
        time = 2
    message:
        "Running Kraken2 on contigs of sample {wildcards.sample}"
    log:
        "logs/{sample}_kraken2_contigs.log"
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        kraken2 --db {params.kraken2_db} --threads {threads} --output {output} --report {params.kraken_report} {input}
        """

rule run_taxonkits_contigs:
    input:
        kraken_out = "kraken2/{sample}.fromcontigs.kraken"
    params:
        taxondb = config["params"]["kraken2"]["taxonkit_db"]
    output:
        taxonkit_tmp = temp("kraken2/{sample}.fromcontigs.taxonkit.tmp"),
        taxonkit_out = "kraken2/{sample}.fromcontigs.taxonkit"
    group: rule_directives.get("run_taxonkits_contigs",{}).get("group",None)
    priority: rule_directives.get("run_taxonkits_contigs",{}).get("priority",None)
    resources:
        cpus = 1,
        mem_mb = 4000
    message:
        "Running TaxonKit on contigs of sample {wildcards.sample}"
    shell:
        """
        cut -f 3 {input.kraken_out} | taxonkit lineage --data-dir {params.taxondb} | taxonkit reformat --data-dir {params.taxondb} --format "k__{{k}}|p__{{p}}|c__{{c}}|o__{{o}}|f__{{f}}|g__{{g}}|s__{{s}}" --miss-rank-repl "unclassified" | cut -f 3 > {output.taxonkit_tmp}
        awk 'BEGIN{{FS="\t"}}; FNR==NR{{ a[FNR""] = $2 FS $3 FS $4; next }}{{ print a[FNR""] FS $0 }}' {input.kraken_out} {output.taxonkit_tmp} > {output.taxonkit_out}
        """

# %% mash rules for best reference identification -------------------

rule run_mash:
    input:
        "Assembly/{sample}/contigs.fa"
    params:
        sketch = "Assembly/{sample}/mash/mash",# only needed if two commands
        mashDB = config["params"]["mash"]["mash_refdb"],
        kmersize = config["params"]["mash"]["mash_kmersize"],
        sketchsize = config["params"]["mash"]["mash_sketchsize"]
    output:
        distance = temp("Assembly/{sample}/mash/mash.dist"),
        topmash = "Assembly/{sample}/mash/mash_best.dist"
    group: rule_directives.get("run_mash",{}).get("group",None)
    priority: rule_directives.get("run_mash",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Running Mash on sample {wildcards.sample}"
    log:
        "logs/{sample}_mash.log"
    shell:
        """
        mash sketch -p {threads} -k {params.kmersize} -s {params.sketchsize} -o {params.sketch} {input} | tee {log} 2> {log}
        mash dist {params.mashDB} {params.sketch}.msh > {output.distance}  | tee -a {log} 2> {log}
        awk 'BEGIN {{OFS="\t"}}; {{split($5,a,"/"); print $1, $2, $3, $4, a[1], a[2]}}' {output.distance} | sort -nr -k 5 > {output.topmash}
        """

rule download_mash_reference:
    input:
        distance = "Assembly/{sample}/mash/mash_best.dist"
    params:
        mashDB = config["params"]["mash"]["mash_refdb"],
        docker = config["smk_params"]["docker"],
        protocol = config["params"]["mash"]["mash_protocol"]  # NOTE: use "true" if host allows ftp connections to the NCBI, it is much faster for gff files
    output:
        ref_fasta = "Assembly/{sample}/ref/reference.fasta",
        ref_gff = "Assembly/{sample}/ref/reference.gff"
    group: rule_directives.get("download_mash_reference",{}).get("group",None)
    priority: rule_directives.get("download_mash_reference",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Downloading Mash reference on sample {wildcards.sample}"
    log:
        "logs/{sample}_mash_download.log"
    shell:
        """
        ## init
        if [ -z "{params.docker}" ]
        then
            reference_genome_db="$(dirname {params.mashDB})/genomes"
        else
            reference_genome_db="$(dirname {input.distance})/genomes"
        fi
        echo "Using reference repository: $reference_genome_db" > {log}
        reference_dict=${{reference_genome_db}}/accession_dict.tsv
        [ ! -d "${{reference_genome_db}}" ] && mkdir -p "$reference_genome_db"

        ## for massive parallel i/o on the dict, copy a backup if $reference_dict gets emptied
        if [ -s "$reference_dict" ] ; then
            :
        elif [ -f "${{reference_dict}}.bak" ] ; then 
            cp -f ${{reference_dict}}.bak $reference_dict
        else 
            touch "$reference_dict"
        fi

        ## retrieve accession id
        accession=$(awk 'NR==1{{print $1}}' {input.distance} | grep -oP '[A-Z]{{2,3}}_[A-Z0-9]+[.]*[0-9]*')
        echo "Best accession is: $accession" >> {log}

        ## query reference repository via dictionary
        MASH_MODE="download"
        grep -q "^$accession" "$reference_dict" && MASH_MODE="copy"
        echo "MASH_MODE: $MASH_MODE" >> {log}

        ## wait if download is pending by another job
        grep -q "^$accession\tawaiting_ncbi_retrieval" "$reference_dict" && echo "The fasta file is currently downloaded by another job. We will wait 2 minutes."&& sleep 120
        grep -q "^$accession\tawaiting_ncbi_retrieval" "$reference_dict" && echo "The fasta file is currently downloaded by another job. We will wait 2 minutes." >> {log} && sleep 120

        ## resort to download if stalled
        if grep -q "^$accession\tawaiting_ncbi_retrieval" "$reference_dict" ; then
          echo "The fasta file did not successfully download or changed its download status. Resorting to download option." >> {log}
          grep -v "^$accession" > "$reference_dict"  # this removes the "pending" status to retry download and avoid duplicates
          MASH_MODE="download"
        fi

        ## resort to download if corrupt
        if grep -q "^$accession\t_genomic.fna.gz" "$reference_dict" ; then
          echo "The $reference_dict is corrupt for the requested accession $accession. Resorting to download option." >> {log}
          grep -v "^$accession" > "$reference_dict"  # this removes the faulty entry to retry download and avoid duplicates
          MASH_MODE="download"
        fi

        if [ "$MASH_MODE" = "copy" ]; then
            fna_ftp_file=$(awk -v accession=$accession 'BEGIN{{FS=OFS="\t"}} {{if ($1 == accession) print $2;}}' "$reference_dict")
            gff_ftp_file=${{fna_ftp_file/.fna.gz/.gff.gz}}
            echo "Copying accession FASTA & GFF files from reference repository: $fna_ftp_file" >> {log}
        fi

        if [ "$MASH_MODE" = "download" ]; then
            echo "The fasta file is not present in the reference repository. Proceeding with download from NCBI." >> {log}
            echo -e "$accession\tawaiting_ncbi_retrieval" >> "$reference_dict"  # this adds an entry to signal a pending process and log incomplete online queries if a fault arises

            ## retrieval of ftp path
            fna_ftp_path=$(esearch -db assembly -query "${{accession}}" | esummary | xtract -pattern DocumentSummary -element FtpPath_RefSeq | awk 'BEGIN{{FS="/"}} END{{print $0"/"$NF"_genomic.fna.gz"}}')
            fna_ftp_file=$(basename $fna_ftp_path)
            gff_ftp_file=${{fna_ftp_file/.fna.gz/.gff.gz}}  # presence of gff file at NCBI is a prerequisite
            gff_ftp_path=$(dirname $fna_ftp_path)/$gff_ftp_file

            ## download with wget to reference repository
            if [[ "{params.protocol}" == "https" ]]
            then  # IMPORTANT: these "nuccore" files are single-fasta/gff3 and may only contain a single chromosome of multi-chromosome species 
                echo "Best accession is $accession" > {log}
                echo "wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=fasta\&id=$accession --tries 10 | gzip > ${{reference_genome_db}}/$fna_ftp_file"  >> {log}
                      wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=fasta\&id=$accession --tries 10 | gzip > ${{reference_genome_db}}/$fna_ftp_file
                echo "wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=gff3\&id=$accession  --tries 10 | gzip > ${{reference_genome_db}}/$gff_ftp_file" >> {log}
                      wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=gff3\&id=$accession  --tries 10 | gzip > ${{reference_genome_db}}/$gff_ftp_file
            else  # IMPORTANT: these "assembly|refseq" files may include multiple chromosomes and associated plasmids in a multi-fasta/gff3 format 
                echo "wget -O ${{reference_genome_db}}/$fna_ftp_file $fna_ftp_path --tries 10" >> {log}
                      wget -O ${{reference_genome_db}}/$fna_ftp_file $fna_ftp_path --tries 10
                echo "wget -O ${{reference_genome_db}}/$gff_ftp_file $gff_ftp_path --tries 10" >> {log}
                      wget -O ${{reference_genome_db}}/$gff_ftp_file $gff_ftp_path --tries 10
            fi
            awk -v accession=$accession -v ftpfile=$fna_ftp_file 'BEGIN{{FS=OFS="\t"}} {{if ($1 == accession) $2=ftpfile;}} 1' "$reference_dict" > tmp.tsv && mv -f tmp.tsv "$reference_dict"
        fi

        ## decompress from reference repository
        gzip --decompress --to-stdout ${{reference_genome_db}}/${{fna_ftp_file}} > {output.ref_fasta}
        gzip --decompress --to-stdout ${{reference_genome_db}}/${{gff_ftp_file}} > {output.ref_gff}
        """

# %% qc rules -------------------------------------------------------

rule run_quast:
    input:
        contigs = "Assembly/{sample}/contigs.fa",
        ref_gff = "Assembly/{sample}/ref/reference.gff",
        ref_fasta = "Assembly/{sample}/ref/reference.fasta"
    params:
        outdir = "Assembly/{sample}/quast/",
        min_contig = config["params"]["quast"]["min_contig"],
        min_identity = config["params"]["quast"]["min_identity"]
    output:
        "Assembly/{sample}/quast/report.tsv",
        "Assembly/{sample}/quast/genome_stats/genome_info.txt",
        "Assembly/{sample}/quast/transposed_report.tsv",
        "Assembly/{sample}/quast/contigs_reports/unaligned_report.tsv",
        "Assembly/{sample}/quast/contigs_reports/transposed_report_misassemblies.tsv"
    # "Assembly/{sample}/quast/busco_stats/short_summary_{sample}.txt"  # BUSCO
    group: rule_directives.get("run_quast",{}).get("group",None)
    priority: rule_directives.get("run_quast",{}).get("priority",None)
    resources:
        cpus = config["smk_params"]["threads"] * config["smk_params"]["thread_factor"],
        mem_mb = 4000,
        # time=lambda wildcards, attempt: 4 * 2 ** attempt  # TODO: wait for bugfix of https://github.com/snakemake/snakemake/issues/1381
        time = 10
    message:
        "Running Quast on sample {wildcards.sample}"
    log:
        "logs/{sample}_quast.log"
    threads:
        config["smk_params"]["threads"] * config["smk_params"]["thread_factor"]
    shell:
        """
        quast --threads {threads} --conserved-genes-finding --output-dir {params.outdir} -r {input.ref_fasta} --features {input.ref_gff} -L {input.contigs} --min-contig {params.min_contig} --min-identity {params.min_identity} | tee {log} 2> {log}
        """

# %% mlst -----------------------------------------------------------

rule run_mlst:
    input:
        contigs = "Assembly/{sample}/contigs.fa"
    params:
        scheme = ('--scheme ' + config["params"]["mlst"]["scheme"]) if config["params"]["mlst"]["scheme"] else '',
        label = "{sample}",
        tmpreport = "Assembly/{sample}/mlst/report.tsv.tmp"
    output:
        report = "Assembly/{sample}/mlst/report.tsv",
        json = "Assembly/{sample}/mlst/report.json"
    group: rule_directives.get("run_mlst",{}).get("group",None)
    priority: rule_directives.get("run_mlst",{}).get("priority",None)
    resources:
        cpus = 1,
        time = 1  # prerequisite: export BLAST_USAGE_REPORT=false
    message:
        "Running MLST on sample {wildcards.sample}"
    log:
        "logs/{sample}_mlst.log"
    shell:
        """
        mlst --json {output.json} --label {params.label} {params.scheme} {input.contigs} > {params.tmpreport} 2> >(tee {log} >&2)
        echo -e "{wildcards.sample}\t$(cut -f 2- {params.tmpreport})" > {output.report}
        """

# %% post-assembly json rules ---------------------------------------

rule parse_json_after_assembly:
    input:
        json_in = "json/pre_assembly/{sample}.aquamis.json" if config["smk_params"]["mode"] != "assembly" else [],
        software_versions = "reports/software_versions.tsv",
        db_hashes = "reports/database_versions.md5",
        assembly = "Assembly/{sample}/contigs.fa",
        assembly_log = "Assembly/{sample}/shovill.log" if config["smk_params"]["mode"] != "assembly" else [],
        samstats = "Assembly/{sample}/vcf/samstats.txt" if config["smk_params"]["mode"] != "assembly" else [],
        samstats_view_count = "Assembly/{sample}/vcf/samstats_view_count.txt" if config["smk_params"]["mode"] != "assembly" else [],
        kraken_contigs_report = "kraken2/{sample}.fromcontigs.taxonkit",
        mash_distance = "Assembly/{sample}/mash/mash_best.dist",
        reference = "Assembly/{sample}/ref/reference.fasta",
        quast_report = "Assembly/{sample}/quast/report.tsv",
        quast_genome_info = "Assembly/{sample}/quast/genome_stats/genome_info.txt",
        quast_misassemblies = "Assembly/{sample}/quast/contigs_reports/transposed_report_misassemblies.tsv",
        quast_unaligned = "Assembly/{sample}/quast/contigs_reports/unaligned_report.tsv",
        mlst = "Assembly/{sample}/mlst/report.json"
    params:
        r1 = lambda wildcards: _get_fastx(wildcards,samples_column),
        r2 = lambda wildcards: _get_fastx(wildcards,samples_column.replace("1","2")),
        sample = "{sample}",
        config = config,
        busco = "Assembly/{sample}/quast/busco_stats/short_summary_{sample}.txt"
    output:
        json_out = "json/post_assembly/{sample}.aquamis.json"
    group: rule_directives.get("parse_json_after_assembly",{}).get("group",None)
    priority: rule_directives.get("parse_json_after_assembly",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Aggregating PostAssembly results in JSON format of sample {wildcards.sample}"
    log:
        log = "logs/{sample}_parseJson_afterAssembly.log"
    script:
        f"{workflow.basedir}/scripts/parse_json.py"

# %% post-assembly report rule --------------------------------------

rule write_report:
    input:
        json_in = expand("json/post_assembly/{sample}.aquamis.json",sample = samples.index),
        fails = "Assembly/fails.txt"
    params:
        tsv_kraken2_contigs = "reports/summary_kraken2_contigs.tsv",
        tsv_assembly = "reports/summary_assembly.tsv",
        tsv_report = "reports/summary_report.tsv",
        tsv_qc_vote = "reports/summary_qc.tsv",
        software_versions = "reports/software_versions.tsv"
    output:
        "reports/assembly_report.html"
    group: rule_directives.get("write_report",{}).get("group",None)
    priority: rule_directives.get("write_report",{}).get("priority",None)
    resources:
        cpus = 1,
        mem_mb = 8000,
        time = 4
    message:
        "Creating Rmarkdown report"
    log:
        log = "logs/write_report.log"
    script:
        f"{workflow.basedir}/scripts/write_report.Rmd"

# %% post-qc report rules -------------------------------------------

rule parse_json_after_qc:
    input:
        json_in = "json/post_assembly/{sample}.aquamis.json",
        software_versions = "reports/software_versions.tsv",
        db_hashes = "reports/database_versions.md5",
        required = "reports/assembly_report.html"
    params:
        sample = "{sample}",
        config = config,
        tsv_qc_vote = "reports/summary_qc.tsv"
    output:
        json_out = "json/post_qc/{sample}.aquamis.json"
    group: rule_directives.get("parse_json_after_qc",{}).get("group",None)
    priority: rule_directives.get("parse_json_after_qc",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Appending QC results to JSON of sample {wildcards.sample}"
    log:
        log = "logs/{sample}_parseJson_afterQC.log"
    script:
        f"{workflow.basedir}/scripts/parse_json.py"

rule filter_json:
    input:
        json_in = "json/post_qc/{sample}.aquamis.json"
    params:
        sample = "{sample}",
        validation = config['params']['json_schema']['validation'],
        filter = config['params']['json_schema']['filter']
    output:
        json_out = "json/filtered/{sample}.aquamis.json"
    group: rule_directives.get("filter_json",{}).get("group",None)
    priority: rule_directives.get("filter_json",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Filtering results according to JSON schema for sample {wildcards.sample}"
    log:
        log = "logs/{sample}_filterJson.log"
    script:
        f"{workflow.basedir}/scripts/filter_json.py"
