# %% assembly rules -------------------------------------------------

# NOTE: single-end mode excludes FLASH analysis

rule run_shovill:
    input:
        r1 = "trimmed/{sample}_R1.fastq.gz",
    params:
        path2bin = f'{workflow.basedir}/scripts/shovill_SingleEnd/shovill',
        depth = config["params"]["shovill"]["depth"],
        outdir = directory("Assembly/{sample}"),
        output_options = config["params"]["shovill"]["output_options"],
        tmpdir = config["params"]["shovill"]["tmpdir"],
        ram = config["params"]["shovill"]["ram"],
        assembler = config["params"]["shovill"]["assembler"],
        kmers = '--kmers "{}"'.format(config["params"]["shovill"]["kmers"]) if config["params"]["shovill"]["kmers"] else "",
        extraopts = '--opts "{}"'.format(config["params"]["shovill"]["extraopts"]) if config["params"]["shovill"]["extraopts"] else "",
        modules = config["params"]["shovill"]["modules"]
    output:
        contigs = "Assembly/{sample}/contigs.fa",
        alignment = temp("Assembly/{sample}/shovill.bam"),
        logfile = ("Assembly/{sample}/shovill.log")
    group: rule_directives.get("run_shovill",{}).get("group",None)
    priority: rule_directives.get("run_shovill",{}).get("priority",None)
    resources:
        cpus = config["smk_params"]["threads"] * config["smk_params"]["thread_factor"],
        mem_mb = config["params"]["shovill"]["ram"] * 1000,
        time = 30
    message:
        "Running Assembly on sample {wildcards.sample}"
    log:
        "logs/{sample}_assembly.log"
    threads:
        config["smk_params"]["threads"] * config["smk_params"]["thread_factor"]
    shell:
        """
        {params.path2bin} {params.extraopts} --keepfiles --force {params.modules} {params.output_options} {params.kmers} --assembler {params.assembler} --depth {params.depth} --tmpdir {params.tmpdir} --cpus {threads} --ram {params.ram} --outdir {params.outdir} --se {input.r1} &> {log}
        """

rule assembly_cleanup:
    input:
        contigs = "Assembly/{sample}/contigs.fa"
    params:
        remove_temp = config["smk_params"]["remove_temp"]
    output:
        contigs = "Assembly/assembly/{sample}.fasta"
    group: rule_directives.get("assembly_cleanup",{}).get("group",None)
    priority: rule_directives.get("assembly_cleanup",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Post-Shovill housekeeping of sample {wildcards.sample}"
    log:
        "logs/{sample}_cleanup.log"
    shell:
        """
        [ -f Assembly/{wildcards.sample}/spades.log ] && mv --verbose Assembly/{wildcards.sample}/spades.log logs/{wildcards.sample}_spades.log > {log}
        [ {params.remove_temp} == "True" ] && rm --verbose -rf Assembly/{wildcards.sample}/spades/ >> {log}
        [ {params.remove_temp} == "True" ] && rm --verbose -f Assembly/{wildcards.sample}/R1.sub.fq.gz >> {log}
        [ {params.remove_temp} == "True" ] && rm --verbose -f Assembly/{wildcards.sample}/R2.sub.fq.gz >> {log}
        cp --verbose {input.contigs} {output.contigs} >> {log}
        """

rule run_samtools:
    input:
        alignment = "Assembly/{sample}/shovill.bam"
    output:
        stats = "Assembly/{sample}/vcf/samstats.txt",
        view_count = "Assembly/{sample}/vcf/samstats_view_count.txt"
    group: rule_directives.get("run_samtools",{}).get("group",None)
    priority: rule_directives.get("run_samtools",{}).get("priority",None)
    resources:
        cpus = 1,
        time = 2
    message:
        "Collecting read mapping statistics of sample {wildcards.sample}"
    log:
        "logs/{sample}_samtools.log"
    shell:
        """
        samtools stats --coverage 1,10000,1 {input.alignment} > {output.stats}
        samtools view {input.alignment} | cut -f 10 | wc -m > {output.view_count}
        """