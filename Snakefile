# %% AQUAMIS: a Snakemake pipeline for Assembly-based QUAlity assessment for Microbial Isolate Sequencing

# Python Standard Packages
import os
from datetime import datetime

# Other Packages
import pandas as pd
import yaml

# %% Settings -------------------------------------------------------

shell.executable("bash")

# Set snakemake main workdir variable
workdir: config["workdir"]

# Import rule directives
with open(config["smk_params"]["rule_directives"],"r") as stream:
    try:
        rule_directives = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        rule_directives = {'rule': ''}

# Collect sample names from sample list
samples_raw = pd.read_csv(config["samples"], sep = "\t", dtype = str, keep_default_na = False).set_index('sample', inplace = False)  # use two steps to set first column as index_col in case samples are integers or a scientific notation
samples = samples_raw[~samples_raw.index.str.match('^#.*')]  # remove samples that are commented-out
samples_column = "fq1" if config["smk_params"]["mode"] != "assembly" else "assembly"


# %% Functions ------------------------------------------------------

def _get_fastx(wildcards, column_name = samples_column):
    return samples.loc[wildcards.sample, [column_name]].dropna()[0]


# %% All and Local Rules --------------------------------------------

localrules: all,all_trimming_only,all_ephemeral,all_post_assembly_qc_only,all_json
localrules: software_versions,database_versions,init_fails_file,delete_ephemeral_sample,delete_ephemeral_dirs

rule all:
    input:
        # Pre-Assembly Results
        "reports/software_versions.tsv",# software_versions
        "reports/database_versions.md5",# database_versions
        expand("trimmed/reports/{sample}.json",sample = samples.index),# run_fastp
        expand("confindr/{sample}/confindr_report.csv",sample = samples.index),# run_confindr
        expand("kraken2/{sample}.species.bracken",sample = samples.index),# run_braken_reads_species
        expand("kraken2/{sample}.genus.bracken",sample = samples.index),# run_braken_reads_genus
        expand("json/pre_assembly/{sample}.aquamis.json",sample = samples.index),# parse_json_before_assembly
        expand("Assembly/assembly/{sample}.fasta",sample = samples.index),# assembly_cleanup
        "reports/fastp_report.html",# write_pre_assembly_report

        # Post-Assembly Results
        expand("Assembly/{sample}/vcf/samstats.txt",sample = samples.index),# run_samtools
        expand("Assembly/{sample}/mash/mash_best.dist",sample = samples.index),# run_mash
        expand("kraken2/{sample}.fromcontigs.taxonkit",sample = samples.index),# run_taxonkits_contigs
        expand("Assembly/{sample}/quast/report.tsv",sample = samples.index),# run_quast
        expand("Assembly/{sample}/quast/genome_stats/genome_info.txt",sample = samples.index),# run_quast
        expand("Assembly/{sample}/mlst/report.json",sample = samples.index),# run_mlst
        expand("json/post_assembly/{sample}.aquamis.json",sample = samples.index),# parse_json_after_assembly
        "reports/assembly_report.html",# write_report
        expand("json/post_qc/{sample}.aquamis.json",sample = samples.index),# parse_json_after_qc
        expand("json/filtered/{sample}.aquamis.json",sample = samples.index),# filter_json

rule all_trimming_only:
    input:
        # Pre-Assembly Results
        "reports/software_versions.tsv",
        "reports/database_versions.md5",
        expand("trimmed/reports/{sample}.json",sample = samples.index),
        expand("confindr/{sample}/confindr_report.csv",sample = samples.index),
        expand("kraken2/{sample}.species.bracken",sample = samples.index),
        expand("kraken2/{sample}.genus.bracken",sample = samples.index),
        expand("json/pre_assembly/{sample}.aquamis.json",sample = samples.index),
        "reports/fastp_report.html"

rule all_post_assembly_qc_only:
    input:
        # Pre-Assembly Results
        "reports/software_versions.tsv",
        "reports/database_versions.md5",

        # Post-Assembly Results
        expand("Assembly/{sample}/mash/mash_best.dist",sample = samples.index),# run_mash
        expand("kraken2/{sample}.fromcontigs.taxonkit",sample = samples.index),# run_taxonkits_contigs
        expand("Assembly/{sample}/quast/report.tsv",sample = samples.index),# run_quast
        expand("Assembly/{sample}/quast/genome_stats/genome_info.txt",sample = samples.index),# run_quast
        expand("Assembly/{sample}/mlst/report.json",sample = samples.index),# run_mlst
        expand("json/post_assembly/{sample}.aquamis.json",sample = samples.index),# parse_json_after_assembly
        "reports/assembly_report.html",# write_report
        expand("json/post_qc/{sample}.aquamis.json",sample = samples.index),# parse_json_after_qc
        expand("json/filtered/{sample}.aquamis.json",sample = samples.index),# filter_json

rule all_ephemeral:
    input:
        # Pre-Assembly Results
        "reports/software_versions.tsv",
        "reports/database_versions.md5",
        expand("json/pre_assembly/{sample}.aquamis.json",sample = samples.index),
        "reports/fastp_report.html",# Comment-out to run on HighTroughput Mode

        # Post-Assembly Results
        expand("json/post_assembly/{sample}.aquamis.json",sample = samples.index),
        expand("logs/{sample}_ephemeral_data.log",sample = samples.index),
        expand("json/post_qc/{sample}.aquamis.json",sample = samples.index),# Comment-out to run on High Throughput Mode
        expand("json/filtered/{sample}.aquamis.json",sample = samples.index),# Comment-out to run on High Throughput Mode
        "reports/assembly_report.html",# Comment-out to run on High Throughput Mode

        # Receipts for Deletion of Ephemeral Data
        "reports/data_availability.log"

rule all_json:
    input:
        # Pre-Assembly Results
        expand("json/pre_assembly/{sample}.aquamis.json",sample = samples.index),

        # Post-Assembly Results
        expand("json/post_assembly/{sample}.aquamis.json",sample = samples.index),
        expand("logs/{sample}_ephemeral_data.log",sample = samples.index),
        expand("json/post_qc/{sample}.aquamis.json",sample = samples.index),# Comment-out to run on High Throughput Mode

        # Receipts for Deletion of Ephemeral Data
        "reports/data_availability.log"

rule all_mash_download:
    input:
        expand("Assembly/{sample}/ref/reference.fasta",sample = samples.index)

# %% Software and Database Version Rules ----------------------------

rule software_versions:
    output:
        versions = "reports/software_versions.tsv"
    params:
        shovill_versions = "reports/shovill_versions.tsv",
    group: rule_directives.get("software_versions",{}).get("group",None)
    priority: rule_directives.get("software_versions",{}).get("priority",None)
    message:
        "Parsing software versions"
    log:
        "logs/software_versions.log"
    shell:
        """
        shovill --check 2> {params.shovill_versions}
        echo -e "Software\tVersion" > {output.versions}
        fastp --version 2>> {output.versions}
        confindr --version >> {output.versions}
        kraken2 --version | head -n 1 | sed 's/ version//' >> {output.versions}
        grep VERSION $(which bracken | head -n 1) | sed -E 's/VERSION="(.*)"/bracken \\1/' >> {output.versions}
        taxonkit version >> {output.versions}
        shovill --version >> {output.versions}
        perl -pe 's/\[shovill\] Using (.+) - .*? \| .*?(\d+\.\d+\.*\d*-*\w*).*/\\1\t\\2/' {params.shovill_versions} >> {output.versions}
        echo -e "mash\t$(mash --version)" >> {output.versions}
        quast --version | grep "QUAST" >> {output.versions}
        mlst --version >> {output.versions}
        blastn -version | grep -P "^blastn" | tr -d ':+' >> {output.versions}
        sed -E 's/ [v]*/\t/' -i {output.versions}
        rm {params.shovill_versions}
        """

rule database_versions:
    input:
        db_kraken = config["params"]["kraken2"]["db_kraken"] + '/hash.k2d',
        db_mash = config["params"]["mash"]["mash_refdb"],
        json_thresholds = config["params"]["qc"]["thresholds"],
        json_schema = config["params"]["json_schema"]["validation"],
        json_filter = config["params"]["json_schema"]["filter"]
    output:
        db_hashes = "reports/database_versions.md5"
    group: rule_directives.get("database_versions",{}).get("group",None)
    priority: rule_directives.get("database_versions",{}).get("priority",None)
    resources:
        cpus = 1,
        time = 4
    message:
        "Parsing database versions"
    log:
        "logs/database_versions.log"
    run:
        import hashlib


        def checksum_md5(filename, buffer_size = (128 * (2 ** 9))):  # read data in 64k chunks
            md5sum = hashlib.md5()
            with open(filename,'rb') as f:
                for chunk in iter(lambda: f.read(buffer_size),b''):
                    md5sum.update(chunk)
            return md5sum.hexdigest()


        hashes = {input.db_kraken      : checksum_md5(filename = input.db_kraken),
                  input.db_mash        : checksum_md5(filename = input.db_mash),
                  input.json_thresholds: checksum_md5(filename = input.json_thresholds),
                  input.json_schema    : checksum_md5(filename = input.json_schema),
                  input.json_filter    : checksum_md5(filename = input.json_filter)
                  }
        with open(output.db_hashes,'w') as output_file:
            {output_file.write("{}\t{}\n".format(value,key)) for (key, value) in hashes.items()}
        print("Databases hashed with MD5.")

rule init_fails_file:
    output:
        "Assembly/fails.txt"
    group: rule_directives.get("init_fails_file",{}).get("group",None)
    priority: rule_directives.get("init_fails_file",{}).get("priority",None)
    message:
        "Creating empty file fails.txt"
    shell:
        "touch {output}"

# %% Import Conditional Rules ---------------------------------------

if config["smk_params"]["mode"] == "assembly":
    include: f"rules/copy_assembly.smk"
else:
    include: f"rules/pre_assembly_{config['smk_params']['mode']}.smk"
    include: f"rules/assembly_{config['smk_params']['mode']}.smk"
include: f"rules/post_assembly.smk"

# %% Deletion of Ephemeral Data -------------------------------------

rule delete_ephemeral_sample:
    input:
        json = "json/post_assembly/{sample}.aquamis.json",
        assembly = "Assembly/assembly/{sample}.fasta"
    params:
        sample = "{sample}",
        ephemeral = config["smk_params"]["ephemeral"]
    output:
        log = "logs/{sample}_ephemeral_data.log"
    group: rule_directives.get("delete_ephemeral_sample",{}).get("group",None)
    priority: rule_directives.get("delete_ephemeral_sample",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Ephemeral Mode - removing files of sample {wildcards.sample}"
    shell:
        """
        echo -e "[$(date +%Y-%m-%d" "%H:%M)]\tThe AQUAMIS pipeline has been executed with the option '--ephemeral'" > {output.log}
        [[ {params.ephemeral} == "True" ]] && [[ -f "trimmed/{params.sample}_R1.fastq.gz"          ]] && rm --verbose -f  "trimmed/{params.sample}"* >> {output.log}  # comment-out to keep trimmed fastq
        [[ {params.ephemeral} == "True" ]] && [[ -d "confindr/{params.sample}"                     ]] && rm --verbose -fR "confindr/{params.sample}" >> {output.log}
        [[ {params.ephemeral} == "True" ]] && [[ -d "Assembly/{params.sample}"                     ]] && rm --verbose -fR "Assembly/{params.sample}" >> {output.log}
        [[ {params.ephemeral} == "True" ]] && [[ -f "kraken2/{params.sample}.fromcontigs.taxonkit" ]] && rm --verbose -f  "kraken2/{params.sample}"* >> {output.log}
        """

rule delete_ephemeral_dirs:
    input:
        logs = expand("logs/{sample}_ephemeral_data.log",sample = samples.index)
    params:
        ephemeral = config["smk_params"]["ephemeral"]
    output:
        receipt = "reports/data_availability.log"
    group: rule_directives.get("delete_ephemeral_dirs",{}).get("group",None)
    priority: rule_directives.get("delete_ephemeral_dirs",{}).get("priority",None)
    resources:
        cpus = 1
    message:
        "Ephemeral Mode - removing directories of all samples"
    log:
        "logs/ephemeral_directories.log"
    shell:
        """
        echo -e "[$(date +%Y-%m-%d" "%H:%M)]\tThe AQUAMIS pipeline has been executed with the option '--ephemeral'" > {output.receipt}
        # [[ {params.ephemeral} == "True" ]] && [[ -d "trimmed"  ]] && rm --verbose -fR trimmed/  >> {output.receipt}  # TODO: comment-in for non-BeONE projects
        [[ {params.ephemeral} == "True" ]] && [[ -d "confindr" ]] && rm --verbose -fR confindr/ >> {output.receipt}
        [[ {params.ephemeral} == "True" ]] && [[ -d "kraken2"  ]] && rm --verbose -fR kraken2/  >> {output.receipt}
        """

# %% Logging Information --------------------------------------------

def pipeline_status_message(status: str) -> tuple:
    RESET = "\033[0m"
    RED = "\033[31m"
    GREEN = "\033[32m"

    timestamp = datetime.now()
    script_id = os.environ.get('script_id', 'unset')
    status_file = os.path.join(config['workdir'], f"pipeline_status_{config['pipeline'].lower()}.txt")
    status_chunk = f"{timestamp.strftime('%F %H:%M')}\t{snakemake.logging.logger.logfile}\t{os.environ.get('CONDA_PREFIX', 'CONDA_PREFIX_is_unset')}\t{os.environ.get('USER', 'unknown')}:{os.uname().nodename}\tShellScriptId:[{script_id}]\n"

    if status == "running":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"[START] {os.path.abspath(__file__)}"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "success":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{GREEN}[SUCCESS]{RESET} Pipeline status: Workflow finished successfully"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "error":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{RED}[ERROR]{RESET} Pipeline status: An error occurred"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"

    return status_file, status, message


onstart:
    status_file, status, message = pipeline_status_message(status = 'running')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onsuccess:
    status_file, status, message = pipeline_status_message(status = 'success')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onerror:
    status_file, status, message = pipeline_status_message(status = 'error')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)
