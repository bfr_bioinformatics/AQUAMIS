AQUAMIS_schema_v20220619.json
-----------------------------

New aquamis.py options and correction for confindr percentage type from integer to number

AQUAMIS_schema_v20220210.json
-----------------------------

First harmonization with EFSA and BeONE data model.


AQUAMIS_schema_v20220125.json
-----------------------------

This schema is based on an AQUAMIS analysis of the SRR498433 test dataset
and the conversion of the full JSON result by the online schema generator
[ExtendsClass](https://extendsclass.com/json-schema-validator.html) and
augmentation with a 'description' field dummy for later redaction.

### Recipe for schema generation

1) paste `AQUAMIS/test_data/json/post_qc/SRR498433.aquamis.json` into JSON box of above URL
2) check box "Options for schema generation: Required"
3) Generate schema from JSON
4) paste JSON schema into new AQUAMIS_schema_vYYYYMMDD.json file
5) PyCharm | Code | Reformat Code (CTRL+ALT+L) with indent=4
6) Regex Steps:
   a)  \s+"default":.* | <delete>
   b)  "properties": \{\s*\} | "properties": {}
   c)  "required": \[\s*\] | "required": []
   d)  "type" | "description": "An explanation about the purpose of this instance.",\n"type"
7) other mods:
   a)  "type": "null" is not allowed: replace e.g. "count_circular_contigs" | from "type": "null" to "type": "integer"
   b)  update "AQUAMIS_schema_vYYYYMMDD.json" keys in file "AQUAMIS_schema_vYYYYMMDD.json"
   optional:
   c) remove items from nucleotide arrays
   d) URL decode into title string
   e) replace root "title": "AQUAMIS JSON Schema" & "description": "version: YYYYMMDD",
8) PyCharm | Code | Reformat Code (CTRL+ALT+L) with indent=4
9) update aquamis.py at default --json_schema and --json_filter
10) update filter_json.py at default create_arguments()
11) delete directory AQUAMIS/test_data/json/filtered
12) rerun AQUAMIS to complete missing files

If file SRR498433.aquamis_validationErrors.json during the above integrity check, relax type from integer to number.

### Schema Filter Integrity Check

1) copy AQUAMIS_schema_vYYYYMMDD.json to AQUAMIS_schema_filter_vYYYYMMDD.json
2) remove any occurrence (n=4) of fields "circular_contigs" in AQUAMIS_schema_**filter**_vYYYYMMDD.json
3) delete directory AQUAMIS/test_data/json/filtered
4) rerun AQUAMIS to complete missing files
5) compare diff of new AQUAMIS/test_data/json/filtered vs. old AQUAMIS/test_data/json/post_qc;
   only the defective (null) metric should be missing
6) correct possible faulty guesses of type, e.g. busco_duplicates=number instead of integer
   until no validation errors occur

AQUAMIS_schema_filter_v20220125.json
------------------------------------
This schema is based on above full schema and was arbitrarily redacted (circular_contigs field removed). It is used as
a positive filter for normalization of full results via the package CERBERUS
in the snakemake rule 'filter_json'.

Json pointer:

* /pipelines/aquamis/report/count_circular_contigs

## Proposed filter for low info-to-size metrics:

Json pointer:

* /pipelines/fastp/insert_size/histogram
* /pipelines/fastp/read1_before_filtering/kmer_count
* /pipelines/fastp/read2_before_filtering/kmer_count
* /pipelines/fastp/read1_after_filtering/kmer_count
* /pipelines/fastp/read2_after_filtering/kmer_count
* /pipelines/samstats/insert_size
* /pipelines/samstats/coverage

This would yield a file size reduction to 1/3 of its unfiltered size.



kraken2_db_hashes.json
----------------------

SHA256 hashes of various kraken database files and their location