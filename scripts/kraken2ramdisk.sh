#!/bin/bash

## HOWTO: execute any sudo command first, e.g. 'sudo ls', before running this script or check presence of file $HOME/.sudo_as_admin_successful
## Note: this script has to reside in AQUAMIS/scripts

## Retrieve script paths and derive source database directory
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
repo_path=$(dirname $script_path)
kraken_path=$(realpath ${repo_path}/reference_db/kraken)

# Create RAMdisk and copy Kraken2 database
[[   -d /mnt/ramdisk ]] && echo "RAMdisk path already exists, skipping mount..."
[[ ! -d /mnt/ramdisk ]] && sudo mount -t tmpfs -o size=8G tmpfs /mnt/ramdisk
[[   -d /mnt/ramdisk/kraken ]] && echo "Kraken2 directory already exists on RAMdisk, skipping mkdir..."
[[ ! -d /mnt/ramdisk/kraken ]] && sudo mkdir /mnt/ramdisk/kraken && sudo chown $USER /mnt/ramdisk/kraken
[[ ! -f $kraken_path/hash.k2d ]] && echo "Kraken2 path '$kraken_path' does not contain a hash.k2d file." && exit 0
[[   -f $kraken_path/hash.k2d ]] && rsync -ruP $kraken_path/* /mnt/ramdisk/kraken/
