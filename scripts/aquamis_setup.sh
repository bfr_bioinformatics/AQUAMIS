#!/bin/bash

## [Goal] Install mamba, conda env, complement conda env, download databases, download test data for AQUAMIS
## [Author] Holger Brendebach
pipeline="AQUAMIS"

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name [OPTIONS]

${bold}Description:${normal}
This script completes the installation of the ${pipeline} pipeline. The openssl library is required for hashing the downloaded files.
For ${pipeline} installations from Bioconda use the option set [--databases] or
For ${pipeline} installations from Gitlab   use the option set [--mamba --databases].
For more information, please visit https://gitlab.com/bfr_bioinformatics/${pipeline,,}

${bold}Options:${normal}
  -s, --status               Show installation status
  -e, --env_name             Override conda environment name derived from envs/${pipeline,,}.yaml (default: ${pipeline,,})
  -m, --mamba                Install the latest version of 'mamba' to the Conda base environment and
                             create the ${pipeline} environment from the git repository recipe
  -b, --busco                Download Augustus and BUSCO databases to <${pipeline}>/download and extract them in the conda environment
  -d, --databases            Download databases to <${pipeline}>/download and extract them in <${pipeline}>/reference_db
  -t, --test_data            Download test data to <${pipeline}>/download and extract them in <${pipeline}>/test_data
  -cdb, --symlinks_to_ceph   ${grey}[BfR-only] Create symbolic links to databases on CephFS${normal}
  -f, --force                Force overwrite for downloads in <${pipeline}>/download
  -k, --keep_downloads       Do not remove downloads after extraction
  -a, --auto                 Do not ask for interactive confirmation, e.g. for tmux-wrapped script calls
  -n, --dryrun               Perform a dryrun to review the commands
  -h, --help                 Show this help

${bold}Example:${normal}
bash $script_real --env_name ${pipeline,,} --mamba --busco --databases --test_data --keep_downloads --dryrun

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname "$script_path")
  conda_recipe="$repo_path/envs/${pipeline,,}.yaml"
  arg_dryrun=""

  ## default values of switches
  conda_status=false
  mamba_status=false
  bioconda_status=false

  arg_mamba=false
  arg_busco=false
  arg_databases=false
  arg_test_data=false
  arg_ceph_symlinks=false
  arg_status=false
  arg_keep_dl=false
  force=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -m | --mamba)
        arg_mamba=true
        shift
        ;;
      -b | --busco)
        arg_busco=true
        shift
        ;;
      -d | --databases)
        arg_databases=true
        shift
        ;;
      -t | --test_data)
        arg_test_data=true
        shift
        ;;
      -cdb | --symlinks_to_ceph)  # this is a BfR-specific option
        arg_ceph_symlinks=true
        shift
        ;;
      -s | --status)
        arg_status=true
        shift
        ;;
      -f | --force)
        force=true
        shift
        ;;
      -k | --keep_downloads)
        arg_keep_dl=true
        shift
        ;;
      -a | --auto)
        interactive=false
        shift
        ;;
      -n | --dryrun)
        dryrun=true
        arg_dryrun=" --dryrun"
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. For the manual, type ${blue}bash $script_real --help${normal}"
  [[ "$arg_databases" == true && "$arg_ceph_symlinks" == true ]] && die "The options --databases and --symlinks_to_ceph are mutually exclusive. Please choose only one."

  ## checks
  [[ $(basename "$script_path") != "scripts" ]] && die "This setup script does not reside in its original installation directory. This is a requirement for proper execution. Aborting..."
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  ## avoid deletion of pre-existing downloads
  [[ -d "$repo_path/download" && $arg_keep_dl == false ]] && logwarn "Skipping default clean-up because download directory already exists: $repo_path/download" && arg_keep_dl=true

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname "$script_real")
script_name=$(basename "$script_real")
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Clean-up on unexpected behaviour

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  [[ -d "$repo_path/download" ]] && [[ -n ${arg_keep_dl+x} ]] && logheader "Clean Up:" && rm -v -R $repo_path/download
  true
}

[[ $(declare -F cleanup) == cleanup && "$arg_keep_dl" == false ]] && logecho "Clean up of download directory is enabled" && trap cleanup SIGINT SIGTERM EXIT ERR

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths()

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 $conda_recipe | cut -d' ' -f2)}

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Keep Downloads:|$arg_keep_dl
Logfile:|$logfile
" | column -t -s="|" | tee -a $logfile $logfile_global
}

## Modules --------------------------------------------------

download_file() {

  # init local variables
  local remote_archive local_archive hashfile extraction_directory
  download_success=false
  remote_archive=$1
  local_archive=$2
  hashfile=$3
  extraction_directory=$4

  # set global variable
  download_success=false

  # Download
  if ( [[ ! -f $local_archive ]] || [[ $force == true ]] ); then
    logecho "Downloading $remote_archive to $local_archive"
    logexec "wget --ca-certificate=$repo_path/resources/seafile-bfr-berlin_certificate.pem --output-document $local_archive $remote_archive" && download_success=true

    # Verify Integrity of Download
    [[ "$download_success" == true  ]] && [[ -s $local_archive ]] && logexec "openssl dgst -r -sha256 $local_archive >> $hashfile"
    [[ "$download_success" == false ]] && logwarn "A download error occurred"
  else
    logwarn "The file $local_archive already exists. Skipping download"
  fi

  if [[ -f $local_archive ]] && ( [[ "$download_success" == true  ]] || [[ $force == true ]] ); then
    # Unpack Downloads
    logecho "Extracting archive $(basename "$local_archive") to $extraction_directory"
    [[ ! -d "$extraction_directory" ]] && logexec mkdir -p $extraction_directory
    logexec tar -xzv -f $local_archive -C $extraction_directory
  fi

  return 0
}

setup_mamba() {
  # checks
  [[ "$bioconda_status" == false ]] && logecho "Installing the latest version of \"mamba\" to the Conda base environment"
  [[ "$bioconda_status" == true  ]] && die "This is a BioConda installation. There is no need to setup Mamba or an environment for ${pipeline}"

  source $conda_base/etc/profile.d/conda.sh
  [[ "$mamba_status" == false ]] && logexec conda install --channel conda-forge mamba
  [[ "$mamba_status" == true ]] && logwarn "Skipping Mamba installation. Mamba is already detected"

  # Create Conda Environment for pipeline
  logecho "Creating the ${pipeline} environment \"${conda_recipe_env_name}\" with the Conda recipe: $conda_recipe"

  set +eu  # workaround: see https://github.com/conda/conda/issues/8186
  [[ -z "${conda_env_path}" ]] && logexec "mamba env create -n ${conda_recipe_env_name} -f $conda_recipe || true"
  [[ -n "${conda_env_path}" && "$force" == true && ! -d "${conda_base}/envs/${conda_env_name}" ]] && logexec "mamba env create -p ${conda_base}/envs/${conda_env_name} -f $conda_recipe || true"  # corner case: there is already a conda env with the same name in another conda_base, e.g. NGSAdmin
  set -eu

  [[ -n "${conda_env_path}" ]] && logwarn "A Conda environment with the name \"$conda_env_name\" is already present. Skipping environment creation" && return 0
  logsuccess "Environment installation completed"
}

complete_busco() {
  # Checks
  [[ -z "${conda_env_path}" ]] && check_conda $conda_recipe_env_name
  [[ -z "${conda_env_path}" ]] && die "No Conda environment for AQUAMIS found to install the QUAST modules AUGUSTUS and BUSCO. Please run $(basename "${BASH_SOURCE[0]}") --mamba first. Aborting..."
  [[ ! -d "$conda_env_path/lib/python3.7/site-packages/quast_libs" ]] && die "The Conda environment installation is incomplete. QUAST was not installed. Aborting..."

  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && logexec mkdir -p $repo_path/download
  hashfile="$repo_path/download/reference_db.sha256"

  # Download files
  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/aquamis_databases/-/raw/main/augustus.tar.gz" # 139MB or https://databay.bfrlab.de/f/fa13c9eb2625477eb729/?dl=1
  local_archive="$repo_path/download/augustus.tar.gz"
  extraction_directory="$conda_env_path/lib/python3.7/site-packages/quast_libs/"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/aquamis_databases/-/raw/main/bacteria.tar.gz" # 8.8MB or https://busco.ezlab.org/v2/datasets/bacteria_odb9.tar.gz
  local_archive="$repo_path/download/bacteria.tar.gz"
  extraction_directory="$conda_env_path/lib/python3.7/site-packages/quast_libs/busco/"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  # Clean up Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $hashfile ]] && logexec "cat $hashfile >> $repo_path/databases/$(basename "$hashfile")"

  logsuccess "BUSCO download completed"
}

download_databases() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && logexec mkdir -p $repo_path/download
  [[ ! -d "$repo_path/reference_db/mash/genomes" ]] && mkdir -p $repo_path/reference_db/mash/genomes
  [[ ! -d "$repo_path/reference_db/taxonkit" ]] && mkdir -p $repo_path/reference_db/taxonkit
  hashfile="$repo_path/download/reference_db.sha256"

  # Download files
  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/aquamis_databases/-/raw/main/confindr_db.tar.gz" # 153MB for the old db incl. some rMLST db's e.g. Enterococcus and Brucella, use https://seafile.bfr.berlin/f/47cae689eda7440c83bb/?dl=1
  local_archive="$repo_path/download/confindr_db.tar.gz"
  extraction_directory="$repo_path/reference_db/"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/aquamis_databases/-/raw/main/mashDB.tar.gz" # 216MB or https://databay.bfrlab.de/f/34b8c88945a8439dac64/?dl=1
  local_archive="$repo_path/download/mashDB.tar.gz"
  extraction_directory="$repo_path/reference_db/mash/"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/aquamis_databases/-/raw/main/minikraken2.tar.gz" # 6.0GB or ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken_8GB_202003.tgz or https://genome-idx.s3.amazonaws.com/kraken/minikraken_8GB_202003.tgz
  local_archive="$repo_path/download/minikraken2.tar.gz"
  extraction_directory="$repo_path/reference_db/"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/aquamis_databases/-/raw/main/taxdump.tar.gz" #  54MB or ftp://ftp.ncbi.nih.gov/pub/taxonomy/taxdump.tar.gz
  local_archive="$repo_path/download/taxdump.tar.gz"
  extraction_directory="$repo_path/reference_db/taxonkit/"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  # Clean up Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $hashfile ]] && logexec "cat $hashfile >> $repo_path/databases/$(basename "$hashfile")"

  logsuccess "Database download completed"
}

symlink_to_ceph_databases() {
  cd "$repo_path/reference_db" || die "Could not change to $repo_path/reference_db"
  [[ -d /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/confindr && ! -d $repo_path/reference_db/confindr ]] && logexec "ln -s /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/confindr confindr"
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/kraken2/minikraken2_v1_8GB && ! -d $repo_path/reference_db/kraken ]] && logexec "ln -s /cephfs/abteilung4/NGS/ReferenceDB/kraken2/minikraken2_v1_8GB kraken"
  [[ ! -d $repo_path/reference_db/mash ]] && logexec "mkdir -p $repo_path/reference_db/mash"
  [[ -f /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/mash/mashDB.msh && ! -f $repo_path/reference_db/mash/mashDB.msh ]] && logexec "ln -s /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/mash/mashDB.msh mash/mashDB.msh"
  [[ -d /cephfs/abteilung4/NGS/Pipelines/aquamis/reference_db/mash/genomes && ! -d $repo_path/reference_db/mash/genomes ]] && logexec "ln -s /cephfs/abteilung4/NGS/Pipelines/aquamis/reference_db/mash/genomes mash/genomes"
  [[ -d /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/taxonkit && ! -d $repo_path/reference_db/taxonkit ]] && logexec "ln -s /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/taxonkit taxonkit"

  logsuccess "Database symbolic linking complete"
}

download_test_data() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && logexec mkdir -p $repo_path/download
  [[ ! -d "$repo_path/test_data/fastq" ]] && logexec mkdir -p $repo_path/test_data/fastq
  hashfile="$repo_path/download/test_data.sha256"

  # Download files
  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/aquamis_databases/-/raw/main/test_data.tar.gz" # 1.1GB
  local_archive="$repo_path/download/test_data.tar.gz"
  extraction_directory="$repo_path/test_data/fastq/"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  # Create Sample Sheet for Test Data
  [[ "$download_success" == true ]] && logecho "Creating test data sample sheet $repo_path/test_data/samples.tsv"
  [[ "$download_success" == true ]] && bash ${script_path}/create_sampleSheet.sh --mode ncbi --fastxDir $repo_path/test_data/fastq --outDir $repo_path/test_data
  echo

  # Clean up Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $hashfile ]] && logexec "cat $hashfile >> $repo_path/databases/$(basename "$hashfile")"

  logsuccess "Test data download completed"
}

check_success() {
  [[ -d ${conda_env_path:-} ]] && status_conda_env="${green}OK${normal}"
  [[ -f $repo_path/reference_db/confindr/refseq.msh ]] && status_confindr="${green}OK${normal}"
  [[ -f $repo_path/reference_db/kraken/hash.k2d ]] && status_kraken="${green}OK${normal}"
  [[ -f $repo_path/reference_db/mash/mashDB.msh ]] && status_mash="${green}OK${normal}"
  [[ -f $repo_path/reference_db/taxonkit/names.dmp ]] && status_taxonkit="${green}OK${normal}"
  [[ -d ${conda_env_path:-$CONDA_PREFIX}/lib/python3.7/site-packages/quast_libs/busco/bacteria ]] && status_busco="${green}OK${normal}"
  [[ -f ${conda_env_path:-$CONDA_PREFIX}/lib/python3.7/site-packages/quast_libs/augustus3.2.3/bin/augustus ]] && status_augustus="${green}OK${normal}"
  logheader "Database Status:"
  echo "
status_conda_env:|${status_conda_env:-"${red}FAIL${normal}"}
status_confindr:|${status_confindr:-"${red}FAIL${normal}"}
status_kraken:|${status_kraken:-"${red}FAIL${normal}"}
status_mash:|${status_mash:-"${red}FAIL${normal}"}
status_taxonkit:|${status_taxonkit:-"${red}FAIL${normal}"}
status_busco:|${status_busco:-"${red}FAIL${normal}"}
status_augustus:|${status_augustus:-"${red}FAIL${normal}"}
" | column -t -s "|"
  echo
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info

## Workflow
check_conda $conda_recipe_env_name
[[ "$interactive" == true ]] && interactive_query
[[ "$arg_mamba" == true ]] && setup_mamba
[[ "$arg_busco" == true ]] && complete_busco
[[ "$arg_databases" == true ]] && download_databases
[[ "$arg_ceph_symlinks" == true ]] && symlink_to_ceph_databases
[[ "$arg_test_data" == true ]] && download_test_data
[[ "$arg_keep_dl" == false ]] && cleanup
[[ "$arg_status" == false ]] && check_conda $conda_recipe_env_name  # check changes to conda
show_info
check_success

logglobal "All steps were logged to: $logfile"
logglobal "[STOP] $script_real"
echo "Thank you for installing ${pipeline}"
