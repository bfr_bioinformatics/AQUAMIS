#!/bin/bash

## [Goal] Create sample sheets for PASS and FAIL fasta files in a folder
## [Author] Holger Brendebach
version=0.2.2

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, # ADD e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name --workdir {path} [Options]

${bold}Description:${normal}
Create sample sheets for PASS and FAIL fasta files from the assembly sub-directory of an AQUAMIS working directory (version: $version).

${bold}Required Named Arguments:${normal}
-d, --workDir {path}            Path to an existing AQUAMIS working directory containing the AQUAMIS results

${bold}Optional Named Arguments:${normal}
-o, --outDir {path}             Path to an existing outDir (default={assemblyDir})
-w, --whitelist {path}          Path to a file containing whitelisted samples, one name per row (default=NA)
-F, --force                     Overwrite existing samples.tsv files in OutDir (default=false)
-n, --dryrun                    Perform a dryrun to review the commands (default=false)
-i, --interactive               Ask before starting the program (default=false)

${bold}Example:${normal}
bash $script_name --workdir {path}
> This saves all PASS samples in the sample sheet {path}/Assembly/assembly/samples_assembly.pass.tsv

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  mode="assembly"
  status="PASS"

  ## default values of switches
  force=false
  dryrun=false
  interactive=false

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -d | --workDir)
        workDir="$2"
        shift 2
        ;;
      -w | --whitelist)
        whitelist_file="$2"
        shift 2
        ;;
      -o | --outDir)
        outDir="$2"
        shift 2
        ;;
      -F | --force)
        force=true
        shift
        ;;
      -n | --dryrun)
        dryrun=true
        shift
        ;;
      -i | --interactive)
        interactive=true
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required params and arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. Try ${blue}bash $script_real --help${normal}"
  [[ ! -d ${workDir:-} ]] && die "The workDir directory does NOT exist: ${workDir:-}"
  [[ -n ${whitelist_file:-} ]] && [[ ! -f ${whitelist_file:-} ]] && die "The whitelist_file file does NOT exist: ${whitelist_file:-}"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  #  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
#[[ $(declare -F error_handler) == error_handler ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM ERR  # TODO: enable if needed, disable in BfR-ABC repositories

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths() use realpaths for proper awk splitting

  report_file="${workDir}/reports/summary_report.tsv"
  assemblyDir="${workDir}/Assembly/assembly"
  outDir=${outDir:-${assemblyDir}}
  outfile_pass="${outDir}/samples_assembly.pass.tsv"
  outfile_fail="${outDir}/samples_assembly.fail.tsv"
  [[ ${dryrun} == true ]] && outfile_pass="/dev/null" && outfile_fail="/dev/null"

  ## checks
  [[ -f $report_file ]] || die "The report_file file does NOT exist: $report_file"
  [[ -d $assemblyDir ]] || die "The assemblyDir directory does NOT exist: $assemblyDir"
  [[ -d $outDir ]] || die "The outDir directory does NOT exist: $outDir"
  [[ -f ${outfile_pass} && ${force} != true ]] && die "The PASS sample sheet already exists. Use --force option to replace it: $outfile_pass"
  [[ -f ${outfile_fail} && ${force} != true ]] && die "The FAIL sample sheet already exists. Use --force option to replace it: $outfile_fail"

  ## logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="${outDir}/${logfile_name}"
  [[ $dryrun == false ]] && logfile_global=/dev/null # ADD

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
workDir:|$workDir
assemblyDir:|$assemblyDir
whitelist_file:|${whitelist_file:-NA}
outfile:|$outfile_pass
logfile:|$logfile
force:|$force
dryrun:|$dryrun
interactive:|$interactive
" | column -t -s "|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  echo
}

## Modules --------------------------------------------------

check_mode() {
  ## [Rationale] check_mode() set find and parsing functions accordingly
  mode="assembly"
  header="sample\tassembly"
  pattern="*.fasta"
  function get_sample_name() { sed 's/\.fasta//'; }
  return 0
}

read_qc_votes() {
  ## [Rationale] read_qc_votes() reads the QC votes from the summary report
  [[ -z "${whitelist_file:-}" ]] && whitelist=false
  [[ -n "${whitelist_file:-}" ]] && mapfile -t whitelist < "$whitelist_file"
  samples_found_whitelist=0

  while read -r sample qc more; do
    if [[ "${whitelist[*]}" != false ]]; then
      skip=false
      for e in "${whitelist[@]}"; do
        if [[ $e == $sample ]]; then
          [[ $dryrun == true ]] && echo -e "$sample\t${bold}whitelisted${normal}"
          ((samples_found_whitelist++))
          sample_pass+=("$sample")
          skip=true
          break
        fi
      done
      [ "$skip" = true ] && continue
    fi
    [[ $qc == "PASS" ]] && sample_pass+=("$sample") && continue
    [[ $qc == "FAIL" ]] && sample_fail+=("$sample") && continue
    logwarn "Unknown QC status: $qc"
  done < <(tail -n +2 ${report_file})
  [[ -z "${sample_pass[*]-}" ]] && die "No PASS samples found"
  return 0
}

create_sample_sheet() {
  ## [Rationale] create_sample_sheet() use create_sample_sheet.sh to create a temporary assembly sample sheet

  ## find files
  files=$(find ${assemblyDir}/* -maxdepth 0 -type f,l -name "${pattern}" -print)
  [[ $(grep -c -v '^$' <<< $files) -eq 0 ]] && die "No assemblies found. Check if your pipeline has run successfully"

  ## write sample sheet header
  [[ $dryrun == true ]] && logheader "Sample Sheet:" && echo -e "$header"
  echo -e "$header" | tee ${outfile_pass} ${outfile_fail} > /dev/null

  ## write sample sheet content
  samples_found_pass=0
  samples_found_fail=0

  for file in $files; do
    unset sample sample_line
    sample=$(basename ${file} | get_sample_name)
    sample_line=$(echo -e "$sample\t${file//\/\//\/}")
    for pass in "${sample_pass[@]}"; do
      if [[ $pass == $sample ]]; then
        echo "$sample_line" >> ${outfile_pass}
        [[ $dryrun == true ]] && echo -e "${sample_line}\t${green}PASS${normal}"
        ((samples_found_pass++))  # TODO: this causes an exit when set -e is enabled
        break
      fi
    done
    for fail in "${sample_fail[@]}"; do
      if [[ $fail == $sample ]]; then
        echo "$sample_line" >> ${outfile_fail}
        [[ $dryrun == true ]] && echo -e "${sample_line}\t${red}FAIL${normal}"
        ((samples_found_fail++))  # TODO: this causes an exit when set -e is enabled
        break
      fi
    done
  done

  [[ $samples_found_fail -eq 0 && -f "$outfile_fail" ]] && rm "$outfile_fail"

  return 0
}

validate_sample_sheet() {
  # check if sample name is contained in the read files
  while read -r sample read1 read2; do
    if ! [[ ${read1} =~ "${sample}_" || ${read1} =~ "${sample}.1.fastq" || ${read1} =~ "${sample}.fasta" || ${read1} =~ "${sample}.fna" ]]; then
      logerror "$sample not contained in $read1"
      status="FAIL"
    fi
  done < <(tail -n +2 ${outfile_pass})

  # check if same sample name more than once
  dups=$(tail -n +2 ${outfile_pass} | cut -f 1 | sort | uniq -c | sed -E 's/^[ ]+//' | cut -f 1 -d ' ' | awk '$1 != 1' | wc -l)
  [[ $dups -gt 0 ]] && status="FAIL" && logerror "Duplicated sample names. Check if your samples are formatted according to the selected mode"

  # report
  [[ $status == "FAIL" ]] && logerror "Check NOT successful. Removing sample sheet: ${outfile_pass}"
  [[ $status == "FAIL" ]] && logexec rm ${outfile_pass} ${outfile_fail}

  return 0
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
check_mode
show_info
[[ $interactive == true ]] && interactive_query # from helper_functions.sh

## Workflow
declare -a sample_pass
declare -a sample_fail
read_qc_votes
create_sample_sheet
validate_sample_sheet

## Summary
if [[ $samples_found_pass -gt 0 ]]; then
  [[ $status == PASS ]] && logsuccess "Found $samples_found_pass PASS samples and $samples_found_fail FAIL samples"
  [[ $status == PASS && $samples_found_whitelist -gt 0 ]] && logsuccess "The PASS samples contain $samples_found_whitelist whitelisted samples"
else
  [[ $dryrun == false ]] && logwarn "No specific samples found. Removing sample sheet: ${outfile_pass}" && rm --verbose ${outfile_pass}
fi

logglobal "[STOP] $script_real"
exit 0
