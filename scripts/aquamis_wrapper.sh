#!/bin/bash

## [Goal] Run AQUAMIS in batches; Wrapper for aquamis.py, direct Snakemake calls or Docker container
## [Author] Holger Brendebach
pipeline="AQUAMIS"

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name

${bold}Description:${normal}
Run ${pipeline} in batch mode based on entries in the ${pipeline,,}_runs.tsv table.
Any script arguments that do not match the ones in the next section will be appended to the run command, e.g. "--unlock".
This wrapper may evaluate several run strategies in its run_modules() function: ${pipeline,,}.py, direct Snakemake calls or Docker container runs.

${bold}Optional Named Arguments:${normal}
  -e, --env_name             Override conda environment name derived from envs/${pipeline,,}.yaml (default=${pipeline,,})
  -p, --profile              Set a Snakemake profile, e.g. workstation (default=bfr.hpc)
  -o, --onerun               Deactivate batch mode and use hardcoded single run parameters (default=false)
  -s, --snakemake            Skip the ${pipeline,,}.py wrapper script and use a customized Snakemake command; this also skips config_${pipeline,,}.yaml recreation
  -cdb, --use_ceph_db        ${grey}[BfR-only] Use the databases on CephFS${normal}
  -cex, --use_ceph_exec      ${grey}[BfR-only] Use pipeline repository on CephFS for executing ${pipeline,,}.py${normal}
  --dryrun                   Perform a dryrun and include a Snakemake dryrun for DAG calculation (default=false)
  -n, --norun                Perform a dryrun without Snakemake commands (default=false), implies --auto
  -a, --auto                 Skip interactive confirmation dialogue (default=false), e.g. for tmux-wrapped script calls

${bold}Notes:${normal}
Check the manual_override() function to tweak script arguments for repetitive use.
Check the tweak_settings() function to review potential system-specific expert settings.
If you like script fail notifications by email, set the environment variable "email" to activate the error_handler() function.

${bold}Example:${normal}
bash $script_name -p bfr.hpc -a -n

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname "$script_path")
  profile="bfr.hpc"
  conda_recipe="$repo_path/envs/${pipeline,,}.yaml"
  dag_search="$repo_path/profiles/smk_directives.yaml"
  docker_image="bfrbioinformatics/aquamis:1.3.12"

  ## default values of switches
  mode="batch"
  snakemake=false
  use_ceph_db=false
  use_ceph_exec=false
  norun=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -p | --profile)
        profile="$2"
        shift 2
        ;;
      -o | --onerun)
        mode="single"
        shift
        ;;
      -s | --snakemake)
        snakemake=true
        shift
        ;;
      -cdb | --use_ceph_db)  # BfR-specific
        use_ceph_db=true
        shift
        ;;
      -cex | --use_ceph_executable)  # BfR-specific
        use_ceph_exec=true
        shift
        ;;
      -n | --norun)
        norun=true
        dryrun=true
        interactive=false
        user_args+="--dryrun "
        shift
        ;;
      --dryrun)
        dryrun=true
        user_args+="--dryrun "
        shift
        ;;
      -a | --auto)
        interactive=false
        shift
        ;;
      -?*)
        user_args+="${1-} "
        shift
        ;;
      *) break ;;
    esac
  done

  ## checks
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname "$script_real")
script_name=$(basename "$script_real")
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
[[ $(declare -F error_handler) == error_handler ]] && [[ -n ${email+x} ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM # ERR # TODO: read HEREDOC causes an unknown error

manual_override() {
  ## (1) Hardcoded Parameters that will override defaults, comment-in to activate

  ## Conda Environment
#  [[ "$profile" == "workstation" ]] && conda_recipe_env_name="aquamis_mamba"  # NOTE: fix for non-standard env names

  ## Code Base
#  repo_path="/home/$USER/miniconda3/envs/aquamis_mamba/opt/aquamis"  # BioConda version
#  repo_path="/cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung"  # tagged version: Accreditation

  ## Redirect to other Databases
  [[ "$use_ceph_db" == true ]] && aquamis_args+=$(cat <<- EOF
    --mashdb /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/mash/mashDB.msh
    --taxonkit_db /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/taxonkit
    --kraken2db /cephfs/abteilung4/NGS/ReferenceDB/kraken2/minikraken2_v1_8GB
    --confindr_database /cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung/reference_db/confindr
EOF
)

  ## Snakemake Profiles (see config.yaml within)
#  profile="denbi.user"
#  profile="denbi.large"
#  profile="nris.saga.slurm.dev"
#  profile="nris.saga.slurm.normal"
#  profile="bfr.hpc"
#  profile="bfr.hpc.greedy"
#  profile="workstation"

  ## Resources
  [[ "$profile" == "bfr.hpc.greedy"         ]] && threads_sample=6
  [[ "$profile" == "bfr.hpc"                ]] && threads_sample=4
  [[ "$profile" == "nris.saga.slurm.normal" ]] && threads_sample=2
  [[ "$profile" == "nris.saga.slurm.dev"    ]] && threads_sample=2
  [[ "$profile" == "denbi.large"            ]] && threads_sample=2
  [[ "$profile" == "denbi.user"             ]] && threads_sample=1
  [[ "$profile" == "workstation"            ]] && threads_sample=1

  ## DAG Job Prioritization
#  dag_search=$repo_path/profiles/smk_directives_depthfirst.yaml
#  dag_search=$repo_path/profiles/smk_directives_breadthfirst.yaml
#  dag_search=$repo_path/profiles/smk_directives_breadthfirst_groups.yaml
#  dag_search=$repo_path/profiles/smk_directives_depthfirst_groups.yaml

  ## Docker Images
#  docker_image="bfrbioinformatics/aquamis:latest"
#  docker_image="bfrbioinformatics/aquamis:1.3.7"

  true
}

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] Manual Run Parameters Definition

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 $conda_recipe | cut -d' ' -f2)}

  ## Resources
  profile_base="$repo_path/profiles"
  profile="$profile_base/$profile"

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  ## Run variables; if in mode "batch" {run_name,dir_samples,dir_data,etc.} will be read from run_table
  run_table="$script_path/${pipeline,,}_runs.tsv"
}

set_run_paths() {
  [[ -d $dir_data                  ]] || logexec mkdir -p "$dir_data"
  logecho "cd $dir_data"
  cd $dir_data || die "Cannot change to working directory: $dir_data"

  [[ -f "$dir_samples/samples.tsv"    ]] && sample_list="$dir_samples/samples.tsv"
  [[ -f "$dir_data/samples_fixed.tsv" ]] && sample_list="$dir_data/samples_fixed.tsv"  # prioritize samples_fixed if it already exists
  [[ -z "${configfile:-}"          ]] && configfile="$dir_data/config_${pipeline,,}.yaml"
  [[ -z "${run_name:-}"            ]] && run_name="test_data_$(date +%F)_$(hostname)_$mode"

  # use Bfr accredited version
  [[ "$use_ceph_exec" == true && -d "/cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung" ]] && repo_path="/cephfs/abteilung4/NGS/Pipelines/aquamis_akkreditierung"

  # checks
  [[ ! -d "$dir_samples" ]] && die "The sample directory does not exist: $dir_samples"
  [[ ! -f "${sample_list:-}" ]] && die "The sample list does not exist: ${sample_list:-unbound_variable}"
  true
}

## Info --------------------------------------------------

show_info() {
  logheader "General Settings:"
  echo "
Mode:|$mode
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Threads per Sample:|$threads_sample
Profile:|$profile
DAG Priority:|$dag_search
Snakemake User Args:|${user_args:-}
Logfile:|$logfile
" | column -t -s="|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  true
}

show_info_on_run() {
  ## print current run infos
  cat <<- TXT | tee -a $logfile $logfile_global

${yellow}#################################################################### Processing:${normal}

Run:           ${inverted}$run_name${normal}
SampleList:    ${bold}${sample_list:-not yet created}${normal}
WorkDir:       ${bold}$dir_data${normal}

${blue}To monitor the Snakemake log use:${normal}
tail -F -s3 \$(find $dir_data/.snakemake/log -name "*.snakemake.log" -print0 | xargs -r -0 ls -1 -t | head -1)
TXT
}

show_info_on_tmux() {
  ## Prerequisite for tmux wrapping: add `set-option -g default-shell "/bin/bash"` to ~/.tmux.conf
  cat <<- TXT | tee -a $logfile $logfile_global

${blue}Best experience with tmux:${normal}
tmux new -d -s ${pipeline,,}_\$USER
tmux send-keys -t ${pipeline,,}_\${USER}.0 'bash $script_real --auto' ENTER

All steps were logged to: $logfile and $logfile_global (global)
TXT
}

## Checks --------------------------------------------------

run_checks() {
  [[ ! -d $profile ]] && die "The snakemake profile directory does not exist: $profile"
  [[ $mode == "batch"  && ! -f $run_table ]] && die "The run table does not exist: $run_table"
  # TODO: add more for novices
  true
}

## More Settings --------------------------------------------------

tweak_settings() {
  ## more privacy towards NCBI
  declare -x BLAST_USAGE_REPORT=false # see https://github.com/tseemann/mlst/issues/115

  ## append environment libraries to LD path, if dependencies are not found
  [[ $profile =~ "saga" ]] && declare -x LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$conda_env_path/lib/ && logecho "Conda libraries appended to LD_LIBRARY_PATH"
  # NOTE: this may cause a "/bin/bash: <conda-prefix>/envs/aquamis/lib/libtinfo.so.6: no version information available (required by /bin/bash)"
  # NOTE: checkout Issues #13 and #15 at https://gitlab.com/bfr_bioinformatics/AQUAMIS/-/issues/?state=all

  ## use krakenDB in ramdisk
  [[ -f /mnt/ramdisk/kraken/hash.k2d ]] && krakenDB="--kraken2db /mnt/ramdisk/kraken" && echo "Using RamDisk copy of kraken2db"
  [[ -f /mnt/ramdisk/kraken/hash.k2d ]] || krakenDB=""

  ## keep referenceDB in sync with server
#  sync_referenceDB
  true
}

activate_conda() {
  ## activate environment
  if [[ "$conda_env_active" != "$conda_env_name" ]]; then
    first_conda_env=$(head -n1 <<< $conda_env_path)  # use first entry if multiple envs with the same name exist, e.g. from other users
    logecho "Conda environment $conda_env_name has not been activated yet. Activating Conda environment \"$first_conda_env\""
    set -u && source $conda_base/etc/profile.d/conda.sh && set +u
    conda activate $first_conda_env
  elif [[ "$use_ceph_exec" == true ]]; then  # BfR-specific
    logwarn "Overriding first conda environment path with accredited CEPH environment path because the flag --use_ceph_executable was set"
    conda_env_path="/home/NGSAdmin/miniconda3/envs/aquamis"
    [[ ! -d "$conda_env_path" ]] && die "The conda environment does not exist: aquamis"
    set -u && source $conda_base/etc/profile.d/conda.sh && set +u
    conda activate $conda_env_path
  else
    logecho "Conda environment $conda_env_name was already activated"
  fi

  ## check
  logecho "Using $(conda info | grep "active env location" | sed -e "s/^\s*//")"
}

## Pre-Run Modules --------------------------------------------------

sync_referenceDB() {
  logexec rsync -avuP "/home/DenekeC/Snakefiles/aquamis/reference_db/confindr/" "$repo_path/reference_db/confindr/"
}

delete_previousRun() {
  logecho "The script function 'delete_previousRun()' is active. Are you sure?"
  select yn in "Yes, I want to delete previous ${pipeline} calculations!" "No, I fear consequences."; do
    case $yn in
      "Yes, I want to delete previous ${pipeline} calculations!" )
        logecho "Starting Cleanup..."
#        [ -d "$dir_data/json_bak"     ] && mv $dir_data/json_bak $dir_data/json_old
#        [ -d "$dir_data/json"         ] && mv $dir_data/json $dir_data/json_9dab9ee
#        [ -d "$dir_data/json"         ] && rm -f -R $dir_data/json
#        [ -d "$dir_data/reports_bak"  ] && mv $dir_data/reports_bak $dir_data/reports_old
#        [ -d "$dir_data/reports"      ] && mv $dir_data/reports $dir_data/reports_9dab9ee
#        [ -d "$dir_data/reports"      ] && rm -f -R $dir_data/reports
#        [ -d "$dir_data/Assembly_bak" ] && mv $dir_data/Assembly_bak $dir_data/Assembly_old
#        [ -d "$dir_data/Assembly"     ] && mv  $dir_data/Assembly $dir_data/Assembly_bak
#        [ -d "$dir_data/Assembly"     ] && rm -f -R $dir_data/Assembly
#        [ -d "$dir_data/.snakemake"   ] && rm -f -R $dir_data/.snakemake
#        [ -d "$dir_data/confindr"     ] && rm -f -R $dir_data/confindr
#        [ -d "$dir_data/kraken2"      ] && rm -f -R $dir_data/kraken2
#        [ -d "$dir_data/logs"         ] && rm -f -R $dir_data/logs
#        [ -d "$dir_data/trimmed"      ] && rm -f -R $dir_data/trimmed
#        [ -f "$dir_data/config.yaml"         ] && mv $dir_data/config.yaml $dir_data/config_9dab9ee.yaml
#        [ -f "$dir_data/config.yaml"         ] && rm -f $dir_data/config.yaml
#        [ -f "$dir_data/pipeline_status.txt" ] && rm -f $dir_data/pipeline_status.txt
#        [ -f "$dir_data/Assembly/fails.txt"  ] && rm -f $dir_data/Assembly/fails.txt
#        [ -f "$dir_data/samples_fixed.tsv"   ] && rm -f $dir_data/samples_fixed.tsv
        echo "Cleanup Finished."
        break
        ;;
      "No, I fear consequences." )
        echo "Cleanup Aborted."
        break
        ;;
    esac
  done < /dev/tty
}

## Modules --------------------------------------------------

run_aquamis() {
  logheader "Running ${pipeline} via ${pipeline,,}.py:"
  aquamis_args+=" --run_name $run_name $krakenDB --fix_fails "
  read -r -d '' wrapper_cmd <<- EOF
  python3 $repo_path/aquamis.py
    --profile $profile
    --working_directory $dir_data
    --rule_directives $dag_search
    --threads_sample $threads_sample
    --sample_list $sample_list
    ${aquamis_args:-}
    ${user_args:-}
EOF
#    --shovill_ram 32
#    --json
#    --profile $profile
#    --shovill_tmpdir /tmp/shovill_$USER  # add this for accreditation version
#    --ephemeral
#    --threads 60
#    --dryrun
#    --iontorrent --shovill_extraopts="--iontorrent"
#    --single_end
#    --shovill_depth 0
#    --remove_temp

#    --force all_trimming_only
#    --version
#    --conda_frontend
#    --condaprefix $CONDA_BASE/envs
  run_command_array $wrapper_cmd
}

run_snakemake() {
  logheader "Running ${pipeline} via direct Snakemake command:"
  # NOTE: for CLI options, see https://snakemake.readthedocs.io/en/stable/executing/cli.html
  [[ -f $dir_data/config_aquamis.yaml ]] || die "The config yaml was not created yet in the working directory"

  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $repo_path/profiles/$profile
    --configfile $configfile
    --snakefile $repo_path/Snakefile
    --notemp
    --dryrun
    --quiet progress
    ${user_args:-}
EOF
#    --rerun-triggers mtime params input software-env code
#    --forcerun download_mash_reference
#    --until download_mash_reference
#    --force all_ephemeral
#    --summary
#    --keep-incomplete
#    --cleanup-metadata  $(snakemake --configfile $configfile --snakefile $repo_path/Snakefile --list-params-changes)
#    --list-input-changes
#    --list-params-changes
#    --list-code-changes
#    --list-version-changes
#    --list-untracked
#    --cleanup-shadow
#    --conda-prefix $CONDA_BASE/envs
#    --use-conda
  run_command_array $wrapper_cmd
}

run_snakemake_report() {
  logheader "Generating Snakemake Report:"
  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $profile
    --configfile $configfile
    --snakefile $repo_path/Snakefile
    --forceall
    --rulegraph | dot -Tpdf -o $dir_data/rulegraph-all.pdf
EOF
#    --dag | dot -Tpdf -o $dir_data/dag-all.pdf
#    --report $dir_data/reports/smk_report.html
  run_command_array $wrapper_cmd
}

run_docker() {
  logheader "Running AQUAMIS via DOCKER:"

  # create sample sheet
  [[ -f $dir_data/samples.tsv ]] || logheader "Creating AQUAMIS Sample Sheet via DOCKER:"
  read -r -d '' wrapper_cmd <<- EOF
  docker run --rm
    -e LOCAL_USER_ID=$(id -u $USER)
    -v ${dir_data}:/AQUAMIS/analysis
    $docker_image
    /AQUAMIS/scripts/create_sampleSheet.sh
    --mode illumina
    --fastxDir /AQUAMIS/analysis/fastq
    --outDir /AQUAMIS/analysis
    ${user_args:-}
EOF
  [[ -f $dir_data/samples.tsv ]] || run_command_array $wrapper_cmd

  # run pipeline
  read -r -d '' wrapper_cmd <<- EOF
  docker run --rm
    -e LOCAL_USER_ID=$(id -u $USER)
    -v $dir_data:/AQUAMIS/analysis
    -v $(realpath $repo_path/reference_db/confindr):/AQUAMIS/reference_db/confindr
    -v $(realpath $repo_path/reference_db/kraken):/AQUAMIS/reference_db/kraken
    -v $(realpath $repo_path/reference_db/mash):/AQUAMIS/reference_db/mash
    -v $(realpath $repo_path/reference_db/taxonkit):/AQUAMIS/reference_db/taxonkit
    -v $(realpath $repo_path/reference_db/trimmomatic):/AQUAMIS/reference_db/trimmomatic
    $docker_image
    /AQUAMIS/aquamis.py
    --docker $dir_data
    --working_directory /AQUAMIS/analysis
    --snakefile /AQUAMIS/Snakefile
    --profile $profile
    --rule_directives /AQUAMIS/profiles/$(basename "$dag_search")
    --threads_sample $threads_sample
    --sample_list /AQUAMIS/analysis/samples.tsv
    --fix_fails
    ${user_args:-}
EOF
  # legacy incompatible options
#    --profile $profile
#    --rule_directives /AQUAMIS/profiles/$(basename "$dag_search")
  # for database mapping when using only bfrbioinformatics/aquamis:conda
#    -v $(realpath $repo_path/reference_db/confindr):/AQUAMIS/reference_db/confindr
#    -v $(realpath $repo_path/reference_db/kraken):/AQUAMIS/reference_db/kraken
#    -v $(realpath $repo_path/reference_db/mash):/AQUAMIS/reference_db/mash
#    -v $(realpath $repo_path/reference_db/taxonkit):/AQUAMIS/reference_db/taxonkit
#    -v $(realpath $repo_path/reference_db/trimmomatic):/AQUAMIS/reference_db/trimmomatic
  run_command_array $wrapper_cmd

  # clean-up
  [[ -f $dir_data/core ]] && rm $dir_data/core
}

run_docker_compose() {
  logheader "Running AQUAMIS via DOCKER-compose:"

  # git image
  app_name="aquamis_app"
  compose_yaml="$repo_path/docker/docker-compose.yaml"

  # bioconda image
  app_name="aquamis_bioconda_app"
  compose_yaml="$repo_path/docker/docker-compose-bioconda.yaml"

  # fire up docker containers
  declare -x LOCAL_USER_ID=$(id -u $USER)
  declare -x AQUAMIS_HOST_PATH=$dir_data
  grep -q $app_name <(docker ps --filter "name=$app_name" --format '{{.Names}}') || logheader "Docker compose UP:"
  grep -q $app_name <(docker ps --filter "name=$app_name" --format '{{.Names}}') || logexec docker compose -f $compose_yaml up --no-build --pull missing --detach

  # create sample sheet
  [[ -f $dir_data/samples.tsv ]] || logheader "Creating AQUAMIS Sample Sheet via DOCKER:"
  read -r -d '' wrapper_cmd <<- EOF
  docker exec --user $LOCAL_USER_ID $app_name
    /AQUAMIS/scripts/create_sampleSheet.sh
    --force
    --mode ncbi
    --fastxDir /AQUAMIS/test_data/fastq
    --outDir /AQUAMIS/analysis
EOF
  [[ -f $dir_data/samples.tsv ]] || run_command_array $wrapper_cmd

  # run pipeline
  read -r -d '' wrapper_cmd <<- EOF
  docker exec --user $LOCAL_USER_ID $app_name
    micromamba run -n aquamis
    /AQUAMIS/aquamis.py
    --docker $dir_data
    --working_directory /AQUAMIS/analysis
    --snakefile /AQUAMIS/Snakefile
    --profile $profile
    --rule_directives /AQUAMIS/profiles/$(basename "$dag_search")
    --threads_sample $threads_sample
    --sample_list /AQUAMIS/analysis/samples.tsv
    --fix_fails
    ${user_args:-}
EOF
  # legacy incompatible options
#    --profile $profile
#    --rule_directives /AQUAMIS/profiles/$(basename "$dag_search")
  # for database mapping when using only bfrbioinformatics/aquamis:conda
#    -v $(realpath $repo_path/reference_db/confindr):/AQUAMIS/reference_db/confindr
#    -v $(realpath $repo_path/reference_db/kraken):/AQUAMIS/reference_db/kraken
#    -v $(realpath $repo_path/reference_db/mash):/AQUAMIS/reference_db/mash
#    -v $(realpath $repo_path/reference_db/taxonkit):/AQUAMIS/reference_db/taxonkit
#    -v $(realpath $repo_path/reference_db/trimmomatic):/AQUAMIS/reference_db/trimmomatic
  run_command_array $wrapper_cmd

  # clean-up
  [[ -f $dir_data/core ]] && rm $dir_data/core
}

run_modules() {
  ## [Rationale] Define per-run module set - choose between Python wrapper, Snakemake or Docker
  set_run_paths
  show_info_on_run
#  delete_previousRun
  [[ $snakemake == false ]] && run_aquamis
  [[ $snakemake == true  ]] && run_snakemake
#  run_snakemake_report
#  run_docker_sample
#  run_docker
#  run_docker_compose
}

## Sample Mode --------------------------------------------------

runs_from_run_table() {
  local run_table

  run_table=${1-}

  ## loop through run_table
  mapfile -t runArray <"$run_table"
  for line in "${runArray[@]}"; do # loop through runs in run_table
    IFS=$'\t' field=($line) IFS=$' \t\n'
    [[ "${field[0]}" =~ ^#.* ]] && continue # skip commented samples
    run_name=$(eval "echo \"${field[0]}\"")
    dir_samples=$(realpath $(eval "echo \"${field[1]}\""))
    dir_data=$(realpath -m $(eval "echo \"${field[2]}\""))
    run_modules
  done

  return 0
}

run_from_hardcoded() {
  dir_samples="$repo_path/test_data"
  dir_data="$repo_path/test_data/analysis_test_data_$(date +%F)"
  run_modules
}

## (3) Main

manual_override
define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info
run_checks

## Workflow
check_conda $conda_recipe_env_name
activate_conda
[[ $interactive == true ]] && interactive_query
tweak_settings

## Switch between single-run or batch mode
case ${mode} in
  batch)
    logecho "Processing all active runs in ${magenta}run table${normal}: $run_table"
    runs_from_run_table "$run_table"
    ;;
  single)
    logecho "Processing a ${magenta}single run${normal}"
    run_from_hardcoded
    ;;
  *) die "The {mode} was not specified or recognized. Check ${blue}bash $script_real --help${normal} for available parameters" ;;
esac

show_info_on_tmux
logglobal "[STOP] $script_real"
