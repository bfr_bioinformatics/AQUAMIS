#!/bin/bash

## [Goal] Create sample sheet for all fastq files in a folder
## [Author] Carlus Deneke, Holger Brendebach
version=0.2.0

## Shell Behaviour --------------------------------------------------
set -Eeuo pipefail # to exit immediately, # ADD e

## (1) Argument Logic

usage() {
  cat <<EOF
${bold}Basic usage:${normal}
bash $script_name [OPTIONS]

${bold}Description:${normal}
This script downloads MASH references that are not provided by the "offline MASH db".
Activation of the associated AQUAMIS conda env is a prerequisite.
For more information, please visit https://gitlab.com/bfr_bioinformatics/AQUAMIS

${bold}Required Named Arguments:${normal}
  -a, --accession            String of a best MASH ID
  -m, --mashDBpath           Path to the MASH db directory, e.g. <AQUAMIS>/reference_db/mash/genomes

${bold}Optional Named Arguments:${normal}
  -d, --ncbiDB               Choose NCBI reference database, either "nuccore" or "refseq" (default=nuccore)
  -v, --verbose              Print script bash debug info
  -h, --help                 Show this help

${bold}Example:${normal}
bash $script_real --accession "NC_000912.1" --mashDBpath $repo_path/reference_db/mash/genomes_test --ncbiDB nuccore

EOF
  exit
}


parse_args() {
  local args=("$@")

  ## default values of variables
  accession=''
  mashDBpath=''
  ncbiDB='nuccore'
  verbose_wget='--no-verbose'

  ## default values of switches
  dryrun=false
  verbose=false

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        verbose=true
        verbose_wget=''
#        set -x
        shift
        ;;
      -a | --accession)
        accession="$2"
        shift 2
        ;;
      -m | --mashDBpath)
        mashDBpath="$2"
        shift 2
        ;;
      -d | --ncbiDB)
        ncbiDB="$2"
        shift 2
        ;;
      -n | --dryrun)
        dryrun=true
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required params and arguments
  [[ $(basename $script_path) != "scripts" ]] && die "This setup script does not reside in its original installation directory. This is a requirement for proper execution."
  [[ -z "${accession-}" ]] && die "Please provide a parameter for the argument --accession. Try ${blue}bash $script_real --help${normal}"
  [[ -z "${mashDBpath-}" ]] && die "Please provide a parameter for the argument --mashDBpath. Try ${blue}bash $script_real --help${normal}"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname $script_real)
script_name=$(basename $script_real)
repo_path=$(dirname $script_path)
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
#[[ $(declare -F error_handler) == error_handler ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM ERR  # TODO: enable if needed

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths() use realpaths for proper awk splitting

  if [[ ${dryrun} == true ]]; then
    if [ -d "/tmp" ] && [ -w "/tmp" ]; then
      mashDBpath="/tmp/${USER}_mashDB" # temporary mashDB directory
      [[ -d "$mashDBpath" ]] || ( mkdir "$mashDBpath" 2> /dev/null && chmod 700 "$mashDBpath" 2> /dev/null ) # create the mashDB directory and set permissions silently
      reference_dict=${mashDBpath}/accession_dict.tsv
      touch "$reference_dict" 2> /dev/null || reference_dict="/dev/null"
    else
      reference_dict="/dev/null"
    fi
  else
    reference_dict=${mashDBpath}/accession_dict.tsv
  fi

  ## checks
  [[ ! -d "${mashDBpath}" ]] && die "The mashDBpath does not exist: $mashDBpath"
  [[ ! -s "$reference_dict" ]] && die "The reference_dict does not exist: $reference_dict"
  [[ -x "$(command -v "esearch")" ]] || die "The esearch command is not available. Please run script in the AQUAMIS conda environment"

  ## logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="${mashDBpath}/${logfile_name}"
  [[ $dryrun == false ]] && logfile_global=/dev/null # ADD

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
accession:|$accession
mashDBpath:|$mashDBpath
reference_dict:|$mashDBpath/accession_dict.tsv
ncbiDB:|$ncbiDB
dryrun:|$dryrun
verbose:|$verbose
" | column -t -s "|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  echo
}

## Modules --------------------------------------------------

download_accession() {

  ## query reference repository via dictionary
  grep -q "^$accession" "$reference_dict" && die "The reference is already present in the dictionary: $reference_dict"
  grep -q "^$accession\tawaiting_ncbi_retrieval" "$reference_dict" && die "The reference is currently downloaded by another job"

  logecho "The fasta file for accession id $accession is not present in the reference repository. Proceeding with download from NCBI $ncbiDB"
  echo -e "$accession\tawaiting_ncbi_retrieval" >> "$reference_dict"  # this adds an entry to signal a pending process and log incomplete online queries if a fault ari

  ## retrieval of ftp path
  fna_ftp_path=$(esearch -db assembly -query "${accession}" | esummary | xtract -pattern DocumentSummary -element FtpPath_RefSeq | awk 'BEGIN{FS="/"} END{print $0"/"$NF"_genomic.fna.gz"}')
  fna_ftp_file=$(basename $fna_ftp_path)
  gff_ftp_file=${fna_ftp_file/.fna.gz/.gff.gz}  # presence of gff file at NCBI is a prerequisite
  gff_ftp_path=$(dirname $fna_ftp_path)/$gff_ftp_file

  ## download with wget to reference repository
  if [[ "$ncbiDB" == "nuccore" ]] ; then  # IMPORTANT: these "nuccore" files are single-fasta/gff3 and may only contain a single chromosome of multi-chromosome species
      logexec "wget ${verbose_wget} -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=fasta\&id=$accession --tries 10 | gzip > ${mashDBpath}/$fna_ftp_file"
      logexec "wget ${verbose_wget} -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=gff3\&id=$accession  --tries 10 | gzip > ${mashDBpath}/$gff_ftp_file"
  elif [[ "$ncbiDB" == "refseq" ]] ; then # IMPORTANT: these "assembly|refseq" files may include multiple chromosomes and associated plasmids in a multi-fasta/gff3 format
      logexec wget ${verbose_wget} -O ${mashDBpath}/$fna_ftp_file $fna_ftp_path --tries 10
      logexec wget ${verbose_wget} -O ${mashDBpath}/$gff_ftp_file $gff_ftp_path --tries 10
  fi

  logecho "Updating the reference dictionary with the file name of the downloaded sequence files"
  awk -v accession=$accession -v ftpfile=$fna_ftp_file 'BEGIN{FS=OFS="\t"} {if ($1 == accession) $2=ftpfile;} 1' "$reference_dict" > tmp.tsv && mv --force tmp.tsv "$reference_dict"
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
[[ $verbose == true ]] && show_info

## Workflow
download_accession

logglobal "[STOP] $script_real"
exit 0
