#!/usr/bin/env python3

# Python Standard Packages
import argparse
import subprocess
import os
import sys

# Other Packages
import pandas as pd
from yaml import dump as yaml_dump


# %% Functions

def argument_parser(pipeline: str = "AQUAMIS",
                    repo_path: str = os.path.dirname(os.path.realpath(__file__))) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    # region # parse arguments
    # path arguments
    parser.add_argument('-l', '--sample_list', default = "/", type = os.path.abspath, help = '[REQUIRED] List of samples to analyze, as a three column tsv file with columns sample and fastq paths. Can be generated with provided script create_sampleSheet.sh')
    parser.add_argument('-d', '--working_directory', default = "/", type = os.path.abspath, help = '[REQUIRED] Working directory where results are saved')
    parser.add_argument('-s', '--snakefile', default = os.path.join(repo_path, "Snakefile"), type = os.path.abspath, help = f'Path to Snakefile of {pipeline}; default: {os.path.join(repo_path, "Snakefile")}'),
    # docker arguments
    parser.add_argument('--docker', default = "", help = 'Mapped volume path of the host system')
    # qc tresholds arguments
    parser.add_argument('--qc_thresholds', default = os.path.join(repo_path, "resources", "AQUAMIS_thresholds.json"), type = os.path.abspath, help = f'Definition of thresholds in JSON file; default: {os.path.join(repo_path, "resources", "AQUAMIS_thresholds.json")}')
    # json schema arguments
    parser.add_argument('--json_schema', default = os.path.join(repo_path, "resources", "AQUAMIS_schema_v20240124.json"), type = os.path.abspath, help = f'JSON schema used for validation; default: {os.path.join(repo_path)}/resources/AQUAMIS_schema_v20240124.json')
    parser.add_argument('--json_filter', default = os.path.join(repo_path, "resources", "AQUAMIS_schema_filter_v20240124.json"), type = os.path.abspath, help = f'Definition of thresholds in JSON file; default: {os.path.join(repo_path, "thresholds", "AQUAMIS_schema_filter_v20240124.json")}')
    # fastp arguments
    parser.add_argument('--min_trimmed_length', default = 15, type = int, help = 'Minimum length of a read to keep; default: 15')
    # mash arguments
    parser.add_argument('--mashdb', default = os.path.join(repo_path, "reference_db", "mash", "mashDB.msh"), type = os.path.abspath, help = f'Path to reference mash database; default: {os.path.join(repo_path, "reference_db", "mash", "mashDB.msh")}')
    parser.add_argument('--mash_kmersize', default = 21, type = int, help = 'kmer size for mash, must match size of database; default: 21')
    parser.add_argument('--mash_sketchsize', default = 1000, type = int, help = 'sketch size for mash, must match size of database; default: 1000')
    parser.add_argument('--mash_protocol', default = "https", choices = ['https', 'ftp'], help = 'Transfer protocol for reference retrieval, choose between https or ftp; default: "https"')
    # kraken arguments
    parser.add_argument('--kraken2db', default = os.path.join(repo_path, "reference_db", "kraken"), type = os.path.abspath, help = f'Path to kraken2 database; default: {os.path.join(repo_path, "reference_db", "kraken2")}')  # Note: minikraken2.tar.gz extracts to "../kraken/" not kraken2
    parser.add_argument('--taxlevel_qc', default = "G", choices = ['S', 'G'], help = 'Taxonomic level for kraken2 classification quality control. Choose S for species or G for genus; default: "G"')
    parser.add_argument('--read_length', default = 150, type = int, help = 'Read length to be used in bracken abundance estimation; default: 150')
    parser.add_argument('--taxonkit_db', default = os.path.join(repo_path, "reference_db", "taxonkit"), type = os.path.abspath, help = f'Path to taxonkit_db; default: {os.path.join(repo_path, "reference_db", "taxonkit")}')
    # confindr database
    parser.add_argument('--confindr_database', default = os.path.join(repo_path, "reference_db", "confindr"), type = os.path.abspath, help = f'Path to ConFindR databases; default: {os.path.join(repo_path, "reference_db", "confindr")}')
    parser.add_argument('--confindr_use_rmlst', default = False, action = 'store_true', help = f'Restrict ConFindR to use only rMLST-derived databases, even when cgMLST-derived exist; default: False')
    # assembly options
    parser.add_argument('--shovill_tmpdir', default = os.path.join(os.sep, "tmp", "shovill_" + str(os.getenv("USER"))), type = os.path.abspath, help = f'Fast temporary directory; default: /tmp/shovill_$USER')
    parser.add_argument('--shovill_ram', default = 16, type = int, help = 'Limit amount of RAM (in GB, integer) provided to shovill; default: 16')
    parser.add_argument('--shovill_depth', default = 100, type = int, help = 'Sub-sample --R1/--R2 to this depth. Disable with --depth 0; default: 100')
    parser.add_argument('--assembler', default = "spades", choices = ['spades', 'skesa', 'megahit', 'velvet'], help = 'Assembler to use in shovill; default: spades')
    parser.add_argument('--shovill_kmers', default = "", help = 'K-mers to use <blank=AUTO>; default: ""')
    parser.add_argument('--shovill_extraopts', default = "", help = 'Extra assembler options in quotes and equal sign notation e.g. spades: --shovill_extraopts="--iontorrent"; default: ""')
    parser.add_argument('--shovill_modules', default = "", help = 'Module options for shovill; choose from --noreadcorr --trim --nostitch --nocorr; default: --noreadcorr; Note: add choices as string in quotation marks')
    parser.add_argument('--shovill_output_options', default = "", help = 'Extra options for shovill; default: ""')
    # mlst options
    parser.add_argument('--mlst_scheme', default = "", help = 'Extra option for MLST; default: ""')
    # quast options
    parser.add_argument('--quast_min_contig', default = 500, type = int, help = 'Extra option for QUAST; default: 500')
    parser.add_argument('--quast_min_identity', default = 80, type = float, help = 'Extra option for QUAST; default: 80')
    # aquamis arguments
    parser.add_argument('-r', '--run_name', default = "", help = 'Name of the sequencing run for all samples in the sample list, e.g. "210401_M02387_0709_000000000-HBXX6", or a self-chosen analysis title')
    parser.add_argument('--no_assembly', default = False, action = 'store_true', help = 'Perform only trimming and contamination assessment on reads')
    parser.add_argument('--assembly_qc', default = False, action = 'store_true', help = 'Perform only QC assessment on FastA assemblies [NOT_RECOMMENDED]')
    parser.add_argument('--single_end', default = False, action = 'store_true', help = 'Use only single-end reads as input')
    parser.add_argument('--iontorrent', default = False, action = 'store_true', help = 'Use single-end Ion Torrent reads as input')
    parser.add_argument('--ephemeral', default = False, action = 'store_true', help = 'Remove most intermediate data. Analysis results are reduced to JSONs and reports')
    parser.add_argument('--json', default = False, action = 'store_true', help = 'Only keep assemblies and JSONs (implies --ephemeral). High-Throughput mode')
    parser.add_argument('--fix_fails', default = False, action = 'store_true', help = 'Re-run Snakemake after failure removing failed samples')
    parser.add_argument('--rule_directives', default = os.path.join(repo_path, "profiles", "smk_directives.yaml"), type = os.path.abspath,
                        help = f'Process DAG with rule grouping and/or rule prioritization via Snakemake rule directives YAML; default: {os.path.join(repo_path, "profiles", "smk_directives.yaml")} equals no directives')
    # conda arguments
    parser.add_argument('--use_conda', default = False, action = 'store_true', help = 'Utilize the Snakemake "--useconda" option, i.e. Smk rules require execution with a specific conda env')
    parser.add_argument('--conda_frontend', default = False, action = 'store_true', help = 'Do not use mamba but conda as frontend to create individual conda environments')
    parser.add_argument('-c', '--condaprefix', default = os.path.join(repo_path, "conda_env"), help = f'Path of default conda environment, enables recycling of built environments; default: {os.path.join(repo_path, "conda_env")}', metavar = 'CONDA_PREFIX,')
    # cpu arguments
    parser.add_argument('--remove_temp', default = False, action = 'store_true', help = 'Remove large temporary files. May lead to slower re-runs but saves disk space')
    parser.add_argument('-t', '--threads', default = 0, type = int, help = f'Number of Threads/Cores to use. This overrides the "<{pipeline}>/profiles" settings')
    parser.add_argument('-p', '--threads_sample', default = 1, type = int, help = 'Number of Threads to use per sample in multi-threaded rules; default: 1', metavar = 'THREADS')
    parser.add_argument('--thread_factor', default = 3, type = int, help = 'Factor to increase threads_sample for shovill and quast; default: 3', metavar = 'THREADS')
    parser.add_argument('-b', '--batch', default = "", type = str, help = 'Compute sample list in batches. Please state a fraction string, e.g. 1/3', metavar = 'FRACTION')
    parser.add_argument('-f', '--force', default = False, type = str, help = 'Snakemake force. Force recalculation of output (rule or file) specified here', metavar = 'RULE')
    parser.add_argument('--forceall', default = False, type = str, help = 'Snakemake force. Force recalculation of all steps', metavar = 'RULE')
    parser.add_argument('--profile', default = "none", type = str, help = f'(A) Full path or (B) directory name under "<{pipeline}>/profiles" of a Snakemake config.yaml with Snakemake parameters, e.g. available CPUs and RAM. Default: "bfr.hpc"')
    parser.add_argument('-n', '--dryrun', default = False, action = 'store_true', help = 'Snakemake dryrun. Only calculate graph without executing anything')
    parser.add_argument('--unlock', default = False, action = 'store_true', help = 'Unlock a Snakemake execution folder if it had been interrupted')
    parser.add_argument('--logdir', default = "/", type = os.path.abspath, help = 'Path to directory where .snakemake output is to be saved')
    parser.add_argument('-V', '--version', default = False, action = 'store_true', help = 'Print program version')
    # endregion # parse arguments
    return parser


def evaluate_arguments(args: argparse.Namespace) -> argparse.Namespace:
    # version
    args.version_fixed = open(os.path.join(args.repo_path, 'VERSION'), 'r').readline().strip()
    try:
        args.version_git = subprocess.check_output(["git", "describe", "--always"], stderr = subprocess.DEVNULL, cwd = args.repo_path).decode('ascii').strip()
    except:
        args.version_git = args.version_fixed

    if args.version:
        print(f"{args.pipeline} version\t{args.version_fixed}")
        if not args.version_fixed == args.version_git:
            print(f"commit  version\t{args.version_git.lstrip('v')}")
        sys.exit(0)
    else:
        print(f"You are using {args.pipeline} version {args.version_fixed} ({args.version_git})")

    # check required arguments
    required_paths_to_check = {
        "-d/--working_directory": args.working_directory,
        "-l/--sample_list"      : args.sample_list,
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if path == '/'}
    if missing_required_paths:
        for key, path in missing_required_paths.items():
            print(f"ERROR: the following argument is required: {key}")
        sys.exit(1)

    # check dependency path
    dependency_paths_to_check = {
        "-l/--sample_list": args.sample_list,
        "--kraken2db"     : args.kraken2db,
        "--taxonkit_db"   : args.taxonkit_db,
        "--mashdb"        : args.mashdb
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
        sys.exit(1)

    # create working directory if it does not exist
    if not os.path.exists(args.working_directory):
        os.makedirs(args.working_directory)

    # set snakemake logdir
    args.logdir = args.working_directory if args.logdir == "/" else args.logdir
    print(f"Snakemake logs are saved in {args.logdir}/.snakemake/log")

    # find snakemake profile: first check relative paths, then absolute paths to make args.profile absolute
    if os.path.exists(os.path.join(args.repo_path, "profiles", args.profile)):
        smk_profile = os.path.join(args.repo_path, "profiles", args.profile)
    elif os.path.exists(os.path.realpath(args.profile)):
        smk_profile = os.path.realpath(args.profile)
    elif os.path.exists(os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.join(args.repo_path, "profiles", "bfr.hpc")):
        smk_profile = os.path.join(args.repo_path, "profiles", "bfr.hpc")
    else:
        print(f"ERROR: path to Snakemake config.yaml {args.profile} does not exist")
        sys.exit(1)
    args.profile = smk_profile

    # other checks
    if args.taxlevel_qc != "S" and args.taxlevel_qc != "G":
        print(f"ERROR: Please specify flag --taxlevel_qc as either S or G. Your choice: {args.taxlevel_qc} is invalid")
        sys.exit(1)
    else:
        print(f"Your taxlevel_qc choice: {args.taxlevel_qc} is valid")

    # adapt execution arguments to single-end or assembly input
    if args.assembly_qc:
        args.mode = "assembly"
    elif args.single_end or args.iontorrent:
        args.mode = "single-end"
        args.shovill_modules = args.shovill_modules.replace("--noreadcorr", "")  # remove incompatible option
    elif args.shovill_modules == "":
        args.mode = "paired-end"
        args.shovill_modules = "--noreadcorr"  # add as default for paired-end

    if args.iontorrent:
        args.shovill_extraopts = (args.shovill_extraopts + " --iontorrent").strip() if not 'iontorrent' in args.shovill_extraopts else args.shovill_extraopts

    # return modified args
    return args


def check_sample_list(sample_list,assembly_qc):
    # Check whether sample list has header
    type1 = "sample\tfq1\tfq2\n"
    type2 = "sample\tassembly\n"
    type3 = "sample\tlong\tshort1\tshort2\n"

    with open(sample_list, 'r') as handle:
        header_line = handle.readline()

    if assembly_qc:
        type_used = type2
    else:
        type_used = type1

    if header_line != type_used:
        print(f"ERROR: sample list {sample_list} does not have a proper header. Make sure that first line is tab-separated:\n{type_used}")
        sys.exit(1)


def create_config(args: argparse.Namespace):
    # region # create config dictionary
    config_dict = {
        'pipeline'   : args.pipeline,
        'version'    : args.version_fixed,
        'git_version': args.version_git,
        'workdir'    : args.working_directory,
        'samples'    : args.sample_list,
        'smk_params' : {
            'log_directory'  : args.logdir,
            'mode'           : args.mode,
            'profile'        : args.profile,
            'rule_directives': args.rule_directives,
            'threads'        : args.threads_sample,
            'thread_factor'  : args.thread_factor,
            'remove_temp'    : args.remove_temp,
            'ephemeral'      : args.json if args.json else args.ephemeral,
            'batch'          : args.batch,
            'docker'         : args.docker
        },
        'params'     : {
            'run_name'   : args.run_name,
            'fastp'      : {'length_required': args.min_trimmed_length},
            'confindr'   : {'database' : args.confindr_database,
                            'use_rmlst': args.confindr_use_rmlst
                            },
            'kraken2'    : {'db_kraken'         : args.kraken2db,
                            'read_length'       : args.read_length,
                            'taxonomic_qc_level': args.taxlevel_qc,
                            'taxonkit_db'       : args.taxonkit_db
                            },
            'shovill'    : {'depth'         : args.shovill_depth,
                            'output_options': args.shovill_output_options,
                            'tmpdir'        : args.shovill_tmpdir,
                            'ram'           : args.shovill_ram,
                            'assembler'     : args.assembler,
                            'kmers'         : args.shovill_kmers,
                            'extraopts'     : args.shovill_extraopts,
                            'modules'       : args.shovill_modules
                            },
            'mash'       : {'mash_refdb'     : args.mashdb,
                            'mash_kmersize'  : args.mash_kmersize,
                            'mash_sketchsize': args.mash_sketchsize,
                            'mash_protocol'  : args.mash_protocol
                            },
            'mlst'       : {'scheme': args.mlst_scheme},
            'quast'      : {'min_contig'  : args.quast_min_contig,
                            'min_identity': args.quast_min_identity
                            },
            'qc'         : {'thresholds': args.qc_thresholds},
            'json_schema': {'validation': args.json_schema,
                            'filter'    : args.json_filter
                            }
        }
    }
    # endregion # create config dictionary

    # write config dictionary to yaml
    with open(args.config_file, 'w') as outfile:
        yaml_dump(data = config_dict, stream = outfile, default_flow_style = False, sort_keys = False)
    print(f"Config written to {args.config_file}")

    return config_dict


def build_snakemake_call(args: argparse.Namespace, sample_subset = False) -> str:
    # force mode
    force_mode = "--forceall" if args.forceall else ""
    force_mode = "--force" if args.force else force_mode

    # user-specified force target; force_targets are mutually exclusive
    force_target = args.forceall if args.forceall else ""
    force_target = args.force if args.force else force_target

    # all-rule force target, add new all rules here
    force_target = "all_trimming_only" if args.no_assembly else force_target
    force_target = "all_post_assembly_qc_only" if args.assembly_qc else force_target
    force_target = "all_ephemeral" if args.ephemeral else force_target
    force_target = "all_json" if args.json else force_target
    force = f"{force_mode} {force_target}"

    # other workflow options
    sample_list = args.sample_list_corrected if sample_subset else args.sample_list
    remove_temp = "" if args.remove_temp else "--notemp"
    batch_rule = "delete_ephemeral_dirs" if args.ephemeral else "write_report"
    batch = f"--batch {batch_rule}={args.batch}" if args.batch else ""
    threads = "--cores" if args.profile == "none" else ""  # use all available cores, if no profile is specified
    threads = f"--cores {args.threads}" if args.threads > 0 else threads  # override cores by commandline arg if provided
    dryrun = "--quiet rules --dryrun" if args.dryrun else ""
    unlock = "--unlock" if args.unlock else ""

    # create command
    command = f"snakemake --profile {args.profile} --snakefile {args.snakefile} --configfile {args.config_file} " \
              f"--config samples={sample_list} {remove_temp} " \
              f"{batch} {threads} {dryrun} {unlock} {force}"

    # append conda options if requested
    if args.use_conda:
        frontend = "conda" if args.conda_frontend else "mamba"
        command = command + f" --use-conda --conda-prefix {args.condaprefix} --conda-frontend {frontend}"

    return command


def find_missing_samples(args):
    samples_raw = pd.read_csv(args.sample_list, index_col = "sample", sep = "\t")
    samples_raw.index = samples_raw.index.astype('str', copy = False)  # in case samples are integers, need to convert them to str
    samples = samples_raw[~samples_raw.index.str.match('^#.*')]  # remove samples that are commented-out

    # find present samples
    present_set = set()
    fix_fails_trigger_sample = "pre_assembly" if args.no_assembly else "post_assembly"
    for s in samples.index:
        if os.path.isfile(os.path.join(args.working_directory, "json", fix_fails_trigger_sample, s + ".aquamis.json")):
            present_set.add(s)

    # write fails to file
    fail_counter = 0
    with open(args.fails_file, "w") as fails_file:
        for sample in samples.index:
            if sample not in present_set:
                fail_counter += 1
                fails_file.write(sample + "\n")
    print(f"Number of FAILED samples: {fail_counter}")

    # exit if none completed
    if len(present_set) == 0:
        print("ERROR: None of the samples completed the pipeline")
        sys.exit(1)
    else:
        print(f"Set of COMPLETE samples: {present_set}")

    # subset pandas dataframe with present samples and write corrected sample sheet
    samples_present = samples.loc[present_set,]
    samples_present.to_csv(args.sample_list_corrected, sep = "\t", index = True)


# %% Main

def main():
    # create commandline parser
    parser = argument_parser()

    # parse arguments and create namespace object
    args = parser.parse_args()

    # append settings to args namespace
    args.pipeline = "AQUAMIS"
    args.repo_path = os.path.dirname(os.path.realpath(__file__))
    args.config_file = os.path.join(args.working_directory, "config_aquamis.yaml")
    args.sample_list_corrected = os.path.join(args.working_directory, "samples_fixed.tsv")
    args.fails_file = os.path.join(args.working_directory, "Assembly", "fails.txt")

    # evaluate arguments
    args = evaluate_arguments(args = args)

    # write config
    config = create_config(args = args)

    # other checks
    check_sample_list(sample_list = config.get('samples'), assembly_qc = args.assembly_qc)

    # run snakemake workflow
    smk_command = build_snakemake_call(args = args)
    print(smk_command)
    subprocess.call(args = smk_command, shell = True, cwd = args.logdir)  # start snakemake from logdir

    # business logic: rerun with filtered sample set if snakemake failed
    if args.fix_fails and not args.dryrun and not args.unlock:
        fix_fails_trigger_completion = "fastp_report.html" if args.no_assembly else "assembly_report.html"
        if not os.path.isfile(os.path.join(args.working_directory, "reports", fix_fails_trigger_completion)):
            print("Re-evaluating samples")

            # subset sample list
            find_missing_samples(args = args)

            # rerun snakemake on sample subset
            smk_command = build_snakemake_call(args = args, sample_subset = True)
            print(smk_command)
            subprocess.call(args = smk_command, shell = True, cwd = args.logdir)  # start snakemake from logdir


# %% Main Call

if __name__ == '__main__':
    main()
